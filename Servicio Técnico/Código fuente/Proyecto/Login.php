<?php
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
    //Aqui definiremos que usuarios pueden entrar
    if (isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true) {
        header('Location: index.php');
    } else {
        echo "Bienvenido a el login!";
        
    }
    
?>


<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <title>Login</title>
    <!-- LIBRERIAS -->
            <?php include("modulosphp/librerias.php");  ?>
        <!-- FIN DE LIBRERIAS -->
    <script src="https://unpkg.com/boxicons@2.0.9/dist/boxicons.js"></script>
</head>

<body class="grad1">
    <div class="container">
        <div class="row">
            <div class="principal col-10">
            <?php
                include("modulosphp/barra.php");

            ?>
        <form method="post" action="loginIniciarS.php">
        <!--aqui se pone todo lo que tiene la caja negra del formulario-->
        <div class="row justify-content-center">
            <div class="row justify-content-center">
                <div class="col-sm-10 col-md-8 col-lg-4 colorTextoSilver text-center tex  colorDivBienvenida m-sm-5 mb-sm-0 p-2 rounded-top sombraForm">
                    <h2>Inicio de sesion</h2>
                </div>
            </div>
            
            <div class="row justify-content-center">
                <div style="margin-bottom: 20px;" class="col-sm-10 col-md-8 col-lg-4 bg-dark text-white mt-sm-0 p-5 pt-4 rounded-bottom sombraForm">
                
                    <div class="row">
                        <div class="row justify-content-center m-0">
                            <label for="Usuario" class="col-form-label col-2  mb-sm-3"><box-icon size="md" name='user' color='#ffffff' ></box-icon></label>
                            <div class="col-12 col-sm-9 ">
                                <input name="txtBoxUs" type="text" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="Nombre de usuario" id="nombres" required>
                            
                            
                            
                            </div>
                        </div>
                        
                        <div class="row justify-content-center m-0">
                            <label for="contraseña" class="col-form-label col-2  mb-sm-3"><box-icon size="md" name='key' color='#ffffff' ></box-icon></label>
                            <div class="col-12 col-sm-9 ">
                                <input name="txtBoxContrasena" type="password" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="Contraseña" id="nombres" required>
                            </div>
                        </div>
                        
    
    
                    </div>
                    <br>
                    <div class="row">
                        <div class="d-grid ">
                            <button type="submit" class="btn colorBoton btn-lg btn-responsive" id="search"> <span class="glyphicon glyphicon-search"></span>Ingresar</button>
                        </div>
                        
                    </div>
                    <br>
                </div>
            </div>
            
            


        </div>


        </form>
            </div>
    </div>
</body>

</html>