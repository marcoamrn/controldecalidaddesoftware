<?php
	session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to=no" >
		<title>Servicio Tecnico</title>
		<!-- LIBRERIAS -->
			<?php include("modulosphp/librerias.php");  ?>
		<!-- FIN DE LIBRERIAS -->		
	</head>
	<body class="grad1">
		<div class="container">
			<div class="row">

			
					<div class="principal col-10">
				
					<!-- MENU -->
						<?php include("modulosphp/barra.php"); ?>
					<!-- FIN MENU -->				

					<!-- PARTE PRINCIPAL -->
						<?php include("modulosphp/principal.php"); ?>
					<!-- FIN PARTE PRINCIPAL -->				

					<!-- CARRUSEL -->
						<?php include("modulosphp/carrusel.php"); ?>
					<!-- FIN CARRUSEL -->					

					<!-- FICHAS -->
						<?php include("modulosphp/fichas.php"); ?>
					<!-- FIN FICHAS -->				
					<section class="margTop py-6">
						<div class="container">
							<h2 class="display-4 mb-4">Nuestro Equipo</h2>
							<div class="row">
								<div class="col-lg-4 col-md-12 text-center my-3">
									<div class="category position-relative carreaux_presentation_light">
										<div class=" category-description position-absolute text-light text-center w-75">
											<h4 class="deroul_titre rounded">Brenda</h4>
											<p class="deroul_soustitre rounded">Estudiante de IT en Buap.</p>
										</div>
										<img src="img/Bren.png" class="img-fluid rounded" alt="Emanuel">
									</div>
								</div>
								<div class="col-lg-4 col-md-12 my-3">
									<div class="category position-relative carreaux_presentation_light">
										<div class="category-description position-absolute text-center text-light w-75">
											<h4 class="deroul_titre rounded">Emanuel</h4>
											<p class="deroul_soustitre rounded">Estudiante de IT en Buap.</p>
										</div>
										<img src="img/Ema.png" class="img-fluid rounded" alt="Marco">
									</div>
								</div>
								<div class="col-lg-4 col-md-12 my-3">
									<div class="category position-relative carreaux_presentation_light">
										<div class="category-description position-absolute text-center text-light w-75 hov-text">
											<h4 class="deroul_titre rounded">Garduño</h4>
											<p class="deroul_soustitre rounded">Estudiante de IT en Buap.</p>
										</div>
										<img src="img/Garduñito.png" class="img-fluid rounded" alt="Miguel">
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- REDES SOCIALES -->
						<?php include("modulosphp/redes.php"); ?>
					<!-- FIN REDES SOCIALES -->
						</div>					
					</div>
				</div>	
			</div>
		</div> <!--Fin del cantainer -->
		

		<!-- JQuery JS -->
		<script type="text/javascript" src="css/bootstrap/js/bootstrap.bundle.js" ></script>
	</body>
</html>