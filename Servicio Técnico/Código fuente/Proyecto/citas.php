<?php
	session_start();
	//Aqui definiremos que usuarios pueden entrar
	if (isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true && $_SESSION['tipoUsuario'] == "Cliente") {
		echo "Bienvenido a la Creacion de citas, " . $_SESSION['usuarioSesion'] . "!";
	} else {
		echo "Please log in first to see this page.";
	}
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
	<!-- jQuery library -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="jsCitas.js"></script>
	<!-- LIBRERIAS -->
	<?php include("modulosphp/librerias.php");  ?>
		<!-- FIN DE LIBRERIAS -->
	
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.css">
</head>
<body  class="grad1">
<div class="container">
		<div class="row">
		
			<div class="principal col-10" style="text-align: left;">
			<?php include("modulosphp/barra.php"); ?>
				<div class="row justify-content-center">
			<div class="row justify-content-center">
                <div class="col-sm-10 col-md-8 col-lg-8 colorTextoSilver text-center tex  colorDivBienvenida m-sm-5 mb-sm-0 p-2 rounded-top sombraForm">
                    <h2>Que servicios necesitaras?</h2>
                </div>
            </div>
		
		<div class="row justify-content-center">
			<div class="col-sm-10 col-md-8 col-lg-8 bg-dark text-white mt-sm-0 p-5 pt-4 rounded-bottom sombraForm">	
				<form action="generarCita.php" method="post">
					
				
				<?php
				if (isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true && $_SESSION['sesionIniciada'] == "Cliente") {
					echo "<p class='text-success'>Bienvenido a la Creacion de citas, " . $_SESSION['usuarioSesion'] . "!</p>";
				} else {
					echo "Please log in first to see this page.";
				}
				
				?>
				<h3>Selecciona tus servicios</h3>
				<div class="form-check form-switch">
					<input id="ckB1" class="form-check-input" type="checkbox" name="serv[]" value="Servicio_Limpieza_Discos" onclick="SumarAcontador(1, 'ckB1', 150); activarDesactivarBtnFecha();">
					<label class="form-check-label">Servicio de Limpieza de disco duro - 1 Hora - 250 Pesos</label>
				</div>
				<div class="form-check form-switch">
					<input id="ckB2" class="form-check-input" type="checkbox" name="serv[]" value="Servicio_Respaldo_Discos_Duros"  onclick="SumarAcontador(2, 'ckB2', 190); activarDesactivarBtnFecha();">
					<label class="form-check-label">Servicio de Respaldo de disco duro - 2 Horas - 190 Pesos</label>
				</div>
				<div class="form-check form-switch">
					<input id="ckB3" class="form-check-input" type="checkbox" name="serv[]" value="Servicio_Cambio_Disco_Duro"  onclick="SumarAcontador(3, 'ckB3', 600); activarDesactivarBtnFecha();">
					<label class="form-check-label">Servicio de Cambio de disco duro - 3 horas - 600 Pesos</label>
				</div>
				<div class="form-check form-switch">
					<input id="ckB4" class="form-check-input" type="checkbox" name="serv[]" value="Servicio_de_Formateo"  onclick="SumarAcontador(2, 'ckB4', 200); activarDesactivarBtnFecha();">
					<label class="form-check-label">Servicio de formateo - 2 Horas - 200 Pesos</label>
				</div>
				<div class="form-check form-switch">
					<input id="ckB5" class="form-check-input" type="checkbox" name="serv[]" value="Servicio_Actualizacion_Sistema_Operativo"  onclick="SumarAcontador(3, 'ckB5', 300); activarDesactivarBtnFecha();">
					<label class="form-check-label">Servicio de actualizacion de sistema operativo - 3 Horas - 300 Pesos</label>
				</div>
				<div class="form-check form-switch">
					<input id="ckB6" class="form-check-input" type="checkbox" name="serv[]" value="Servicio_Actualizacion_Programas"  onclick="SumarAcontador(2, 'ckB6', 200); activarDesactivarBtnFecha();">
					<label class="form-check-label">Servicio de actualizacion de programas - 2 Horas - 200 Pesos</label>
				</div>
				<div class="form-check form-switch">
					<input id="ckB7" class="form-check-input" type="checkbox" name="serv[]" value="Servicio_Instalacion_Antivirus"  onclick="SumarAcontador(1, 'ckB7', 300); activarDesactivarBtnFecha();">
					<label class="form-check-label">Servicio de instalacion de antivirus - 1 Hora - 300 Pesos</label>
				</div>
				<div class="form-check form-switch">
					<input id="ckB8" class="form-check-input" type="checkbox" name="serv[]" value="Servicio_Limpieza_Virus"  onclick="SumarAcontador(1, 'ckB8', 150); activarDesactivarBtnFecha();">
					<label class="form-check-label">Servicio de limpieza de virus - 1 Hora - 150 Pesos</label>
				</div>
				<div id="textoContador">Tu servicio durara: <input class="form-control-plaintext colorTextoSilver colorBorderBottomLight tamTxtbox" type="text" value="0" id="horasServicio" name="horasServicio" readonly="readonly"> Horas.</div>
				<div id="textoContadorPrecio">Tu servicio costara: <input class="form-control-plaintext colorTextoSilver colorBorderBottomLight" type="text" value="0" id="costoServicio" name="costoServicio" readonly="readonly"> pesos.</div>
				
				<h3>Selecciona el dia de tu servicio</h3>
				<br>
				<input id="fechaServicio" class="form-control-plaintext colorTextoSilver colorBorderBottomLight tamTxtbox" type="date" name="fechaServicio" onchange="activarDesactivarBtnFecha();"></input>
				<br>
				<br>
				<input id="btnValF" class="btn btn-light" type="button" name="enviar" value="Verificar fecha" href="javascript:;" onclick="VerifCalendario($('#horasServicio').val(),$('#fechaServicio').val()); limpiarHoraSelec();" disabled>
				<br>
				<br>
				<div id="resultado">Selecciona una fecha para verificar la disponibilidad de horarios</div>
				<!--
				<input id="btnValHora" type="button" name="enviar" value="Seleccionar Hora de inicio" href="javascript:;" onclick="VerifHorario($('#horasServicio').val(),$('#fechaServicio').val());" disabled>
				-->
				<div id="mensajeConfirmacion"></div>
				<div id="resumenServicio"></div>

				
					
				

				<br><br><br><br><br><br><br><br>

				</form>
			</div>
		</div>
	</div>
	</div>
		</div>
	</div>
	<script>
		var today = new Date();
		var fechaD = new String(today.getDate());
		var diaCorrecto = new String();
		console.log("la variable fechaD "+ fechaD +" tiene " + fechaD.length + " caracteres");
		if(fechaD.length === 1){
			diaCorrecto = "0" + fechaD;
		}
		else if(fechaD.length === 2){
			diaCorrecto = fechaD;
		}
		console.log(diaCorrecto);
		var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+diaCorrecto;
		document.getElementById("fechaServicio").value = date;
		document.getElementById("fechaServicio").setAttribute('min', date);
		
	</script>
	
</body>
</html>