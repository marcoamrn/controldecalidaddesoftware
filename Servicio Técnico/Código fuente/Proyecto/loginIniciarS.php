<?php
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
    $txtBoxUs = $_POST["txtBoxUs"];
    $txtBoxContrasena = $_POST["txtBoxContrasena"];
    print($txtBoxUs.$txtBoxContrasena);
    include("conexion.php");
    $link = conectar();
    $query = "SELECT usuario, contrasena, tipoUsuario, idUsuario FROM datosusuario WHERE usuario='".$txtBoxUs."'";
	$consulta = mysqli_query($link, $query);
    $datos = mysqli_num_rows($consulta);
       
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <title>Login</title>
    <script src="https://unpkg.com/boxicons@2.0.9/dist/boxicons.js"></script>
</head>

<body class="grad1">
    <div class="container" >
        <form method="post" action="loginIniciarS.php">
    	<!--aqui se pone todo lo que tiene la caja negra del formulario-->
    	<div class="row justify-content-center">
            <div class="row justify-content-center">
                <div class="col-sm-10 col-md-8 col-lg-4 colorTextoSilver text-center tex  colorDivBienvenida m-sm-5 mb-sm-0 p-2 rounded-top sombraForm">
                    <h2>Inicio de sesion</h2>
                </div>
            </div>
			
			<div class="row justify-content-center">
                <div class="col-sm-10 col-md-8 col-lg-4 bg-dark text-white mt-sm-0 p-5 pt-4 rounded-bottom sombraForm">
    			
                    <div class="row">
                        
                        <?php
                        if($datos > 0){
                            $fila = mysqli_fetch_row($consulta);
                            if($fila[1] == $txtBoxContrasena){
                                $_SESSION["usuarioSesion"] = $txtBoxUs;
                                $_SESSION["sesionIniciada"] = true;
                                $_SESSION["tipoUsuario"] = $fila[2];
                                $_SESSION["idCliente"] = $fila[3];
                                echo "El usuario: ".$_SESSION["usuarioSesion"]." ha iniciado sesion, eres un ".$_SESSION["tipoUsuario"];
                            }
                            else{
                                echo "Datos incorrectos, vuelve al login";
                            }
                        }
                        else{
                            echo "Datos incorrectos, vuelve al login";
                        }
                        $referer = $_SESSION["referer"];
                        print($referer);

                        header('Location: index.php');
                        ?>
    
    
                    </div>
                    <br>
                    
                    <br>
                </div>
            </div>
    		
    		


    	</div>


        </form>
    </div>
</body>

</html>