<a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Login</a>
									<ul class="dropdown-menu login">
										<li>
											<div class="row">
												<div class="col-12">
													<form>
														<div>
															<label class="label_login">Login via ?</label>	
															<a href="#" class="btn btn-primary btn-sm disabled">Usuario</a>
														</div>
														<div>
															<img src="img/avatar-1.png" class="img_log">
														</div>
														<div class="form-group">
															<div class="input-group mb-2">
																<span class="input-group-text"><i class="fa fa-user fa-2x"></i></span>
																<input class="form-control" type="text" placeholder="UserName">
															</div>
														</div>
														<div class="form-group">
															<div class="input-group mb-2">
																<span class="input-group-text"><i class="fa fa-lock fa-2x"></i></span>
																<input class="form-control" type="password" placeholder="Password">
															</div>
															<div class="help-block">
																<a href="#">Forget the Password</a>
															</div>
														</div>
														<div class="form-group">
															<div class="d-grid">
																<button type="submit" class="btn btn-primary btn-block">Sign in</button>
															</div>
														</div>
													</form>		
												</div>	
											</div>
										</li>
									</ul>