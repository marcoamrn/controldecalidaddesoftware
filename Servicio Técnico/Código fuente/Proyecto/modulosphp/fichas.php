<div class="row justify-content-center">
					
					<div class="col-12 col-sm-6 col-md-4 col-lg-3">
						<div class="tabla-precio">
							<div>
								<span class="titulo fondo-bk-t">Discos duros</span>
								<span class="servicios">Servicios: </span>								
							</div>
							<div>
								<ul>
									<li>Limpieza de disco duro</li>
									<li>Respaldo de disco duro</li>
									<li>Cambio de disco duro</li>
								</ul>
								<?php
									if(isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true && $_SESSION['tipoUsuario'] == "Cliente"){
										echo "<a href='citas.php' class='btn btn-outline-dark'>Agendar cita</a>";
									}
									else if(isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == false && $_SESSION['tipoUsuario'] == ""){
										echo "<a href='login.php' class='btn btn-outline-dark'>Agendar cita</a>";
									}
								?>
								
							</div>
						</div>						
					</div>

					<div class="col-12 col-sm-6 col-md-4 col-lg-4">
						<div class="tabla-precio">
							<div>
								<span class="titulo fondo-gr-t">Actualizaciones</span>
								<span class="servicios">Servicios: </span>								
							</div>
							<div>
								<ul>
									<li>Actualizacion de sistema operativo</li>
									<li>Actualizacion de programas</li>
								</ul>
								<?php
									if(isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true && $_SESSION['tipoUsuario'] == "Cliente"){
										echo "<a href='citas.php' style='margin-top:50px' class='btn btn-outline-success'>Agendar cita</a>";
									}
									else if(isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == false && $_SESSION['tipoUsuario'] == ""){
										echo "<a href='login.php' style='margin-top:50px' class='btn btn-outline-success'>Agendar cita</a>";
									}
								?>
								
							</div>
						</div>						
					</div>

					<div class="col-12 col-sm-6 col-md-4 col-lg-3">
						<div class="tabla-precio">
							<div>
								<span class="titulo fondo-rj-t">Antivirus</span>
								<span class="servicios">Servicios: </span>								
							</div>
							<div>
								<ul>
									<li>Limpieza de virus</li>
									<li>Actualizacion de antivirus</li>
									<li>Instalacion de antivirus</li>
								</ul>
								<?php
									if(isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true && $_SESSION['tipoUsuario'] == "Cliente"){
										echo "<a href='citas.php' class='btn btn-outline-danger'>Agendar cita</a>";
									}
									else if(isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == false && $_SESSION['tipoUsuario'] == ""){
										echo "<a href='login.php' class='btn btn-outline-danger'>Agendar cita</a>";
									}
								?>



								
							</div>
						</div>						
					</div>

					
				</div>