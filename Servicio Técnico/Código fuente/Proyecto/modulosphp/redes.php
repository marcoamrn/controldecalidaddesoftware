<div>
					<div class="row">
						<div class="col-12 col-sm-6 col-md-4 col-lg-4">
							<div class="caja">
								<i class="fa fa-instagram fa-4x icono"></i>
								<h4>Instagram</h4>
								<div>
									<span>Aqui puedes encontrar el enlace a los comentarios realizados via <strong>Instagram</strong></span>
								</div>
								<a href="http://www.instagram.com" class="link" >Acceso</a>
							</div>
						</div>

						<div class="col-12 col-sm-6 col-md-4 col-lg-4">
							<div class="caja">
								<i class="fa fa-twitter fa-4x icono"></i>
								<h4>Twitter</h4>
								<div>
									<span>Aqui puedes encontrar el enlace a los comentarios realizados via <strong>Twitter</strong></span>
								</div>
								<a href="http://www.twitter.com" class="link" >Acceso</a>
							</div>
						</div>

						<div class="col-12 col-sm-6 col-md-4 col-lg-4">
							<div class="caja">
								<i class="fa fa-facebook fa-4x icono"></i>
								<h4>Facebook</h4>
								<div>
									<span>Aqui puedes encontrar el enlace a los comentarios realizados via <strong>Facebook</strong></span>
								</div>
								<a href="https://www.facebook.com/Servicios-tecnicos-de-computadoras-111100213982243/" class="link" >Acceso</a>
							</div>
						</div>
