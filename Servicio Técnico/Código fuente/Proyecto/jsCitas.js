var contador = 0;
var contadorCosto = 0;
function SumarAcontador(valorAsumar, idCkB, valorCosto){
		
	
		if(document.getElementById(idCkB).checked){
			contador = parseInt(contador) + parseInt(valorAsumar);
			contadorCosto = parseInt(contadorCosto) + parseInt(valorCosto);

			document.getElementById("horasServicio").setAttribute('value', contador);
			document.getElementById("costoServicio").setAttribute('value', contadorCosto);
		}
		else{
			contador = parseInt(contador) - parseInt(valorAsumar);
			contadorCosto = parseInt(contadorCosto) - parseInt(valorCosto);
			document.getElementById("horasServicio").setAttribute('value', contador);
			
			document.getElementById("costoServicio").setAttribute('value', contadorCosto);
		}
		
	

}
function VerifCalendario(horasServicio,fechaServicio) {
    var parametros = {"HorasServicio":horasServicio, "FechaServicio":fechaServicio};
    $.ajax({
        data:parametros,
        url:'procesarCalCita.php', type: 'post',
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor");
        },
        success: function (response) {   
            $("#resultado").html(response);
        }
    });
}
function VerifHorario(horasServicio,fechaServicio) {
    var parametros = {"HorasServicio":horasServicio, "FechaServicio":fechaServicio};
    $.ajax({
        data:parametros,
        url:'procesarHorarioCita.php', type: 'post',
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor");
        },
        success: function (response) {   
            $("#resultado").html(response);
        }
    });
}
function activarDesactivarBtnFecha(){
	if((document.getElementById("horasServicio").value > 0)){
		document.getElementById("btnValF").disabled = false;
	}
	else{
		document.getElementById("btnValF").disabled = true;
	}
}
function confirmarHora(valorBoton){
	var horaSeleccionada = valorBoton;
	let listaRadioBtns = new Array();
	
	document.getElementById("mensajeConfirmacion").innerHTML = "Seleccionaste las " + "<input class='form-control-plaintext colorTextoSilver colorBorderBottomLight' type='text' value='"+horaSeleccionada+"' id='horaSelec' name='horaSelec' readonly='readonly'> Horas.</div> como hora de inicio de tu cita <br><h2>Resumen de tu servicio</h2>";
	for(var i=1; i <= 8; i++){
		if(document.getElementById("ckB" + i).checked){
			console.log(document.getElementById("ckB" + i).value);
			listaRadioBtns.push(document.getElementById("ckB" + i).value);
		}
	}
	document.getElementById("resumenServicio").innerHTML = "Elegiste los servicios: ";
	listaRadioBtns.forEach(element => {
	 	document.getElementById("resumenServicio").innerHTML = " " + document.getElementById("resumenServicio").innerHTML + element.replaceAll("_", " ") + ", ";
	});
	document.getElementById("resumenServicio").innerHTML = document.getElementById("resumenServicio").innerHTML + " tu servicio tendra una duracion de " + document.getElementById("horasServicio").value + " horas, ";
	document.getElementById("resumenServicio").innerHTML = document.getElementById("resumenServicio").innerHTML + " que tendra lugar el dia " + document.getElementById("fechaServicio").value + ", ";
	document.getElementById("resumenServicio").innerHTML = document.getElementById("resumenServicio").innerHTML + " a las " + horaSeleccionada + " horas, ";
	document.getElementById("resumenServicio").innerHTML = document.getElementById("resumenServicio").innerHTML + " con un costo de " + document.getElementById("costoServicio").value + " pesos, ";
	horaFinal = parseInt(horaSeleccionada.substring(0, 2)) + parseInt(document.getElementById("horasServicio").value);
	document.getElementById("resumenServicio").innerHTML = document.getElementById("resumenServicio").innerHTML + " puedes pasar por tu dispositivo despues de las " + horaFinal + ":00 Hrs o al siguiente dia. <br> <br>";
	document.getElementById("resumenServicio").innerHTML = document.getElementById("resumenServicio").innerHTML + " <button class='btn btn-success' type='submit'>Generar cita</button>";
}
function limpiarHoraSelec(){
	document.getElementById("mensajeConfirmacion").innerHTML = " ";
	document.getElementById("resumenServicio").innerHTML = " ";
}



function abrirSegundoModal(horaElegida){
	var modal2 = document.getElementById("myModal2");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close2")[0];

        // When the user clicks the button, open the modal 
        
        modal2.style.display = "block";
        
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
          modal2.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
          if (event.target == modal2) {
            modal2.style.display = "none";
          }
        }
		var fecha = document.getElementById("fechaElegida").value;
		var parametros = {"HoraElegida":horaElegida , "FechaElegida":fecha};
		$.ajax({
			data:parametros,
			url:'procesarHoraElegida.php', type: 'post',
			beforeSend: function () {
				$("#resultado2").html("Procesando, espere por favor");
				document.getElementById("resultado2").innerText = "hola";
			},
			success: function (response) {   
				$("#resultado2").html(response);
			}
		});
}
function elimCita(idCitaElim){
	var modal2 = document.getElementById("myModal");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks the button, open the modal 
	
	modal2.style.display = "block";
	
	// When the user clicks on <span> (x), close the modal

	var parametros = {"CitaAEliminar":idCitaElim};
	$.ajax({
		data:parametros,
		url:'procesarElimCita.php', type: 'post',
		beforeSend: function () {
			$("#resultado").html("Procesando, espere por favor");
			document.getElementById("resultado2").innerText = "hola";
		},
		success: function (response) {   
			$("#resultado").html(response);
		}
	});
	span.onclick = function() {
		modal2.style.display = "none";
		location.reload();
		
		  alert("refrescar");
	  }
  
	  // When the user clicks anywhere outside of the modal, close it
	  window.onclick = function(event) {
		if (event.target == modal2) {
		  modal2.style.display = "none";
		  location.reload();
		  alert("refrescar");
		}
	  }

}
function MarcarCitaCompletada(){
	var horaSeleccionadaTecnico = document.getElementById("horaSeleccionadaTecnico").innerText;
	var parametros = {"horaSeleccionadaTecnico":horaSeleccionadaTecnico};
	$.ajax({
		data:parametros,
		url:'procesarCompletarCita.php', type: 'post',
		beforeSend: function () {
			$("#estado").html("Procesando, espere por favor");
		},
		success: function (response) {   
			$("#estado").html(response);
		}
	});


}