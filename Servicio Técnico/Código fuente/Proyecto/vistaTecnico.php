<?php
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	//Aqui definiremos que usuarios pueden entrar
	if (isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true) {
		echo "No puedes entrar aqui, ya iniciaste sesion";
	} else {
        echo "Bienvenido a el login!";
		
	}
?>


<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <title>Login</title>
    <!-- LIBRERIAS -->
			<?php include("modulosphp/librerias.php");  ?>
		<!-- FIN DE LIBRERIAS -->
    <script src="https://unpkg.com/boxicons@2.0.9/dist/boxicons.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--web-fonts-->
<link href='//fonts.googleapis.com/css?family=Romanesco' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:400,500,100,100italic,300,300italic,500italic,700,700italic,900,900italic,400italic' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<!--Calender-->
<script src="js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="css/clndr.css" type="text/css" />
<script src="js/underscore-min.js"></script>
<script src= "js/moment-2.2.1.js"></script>
<script src="js/clndr.js"></script>
<script src="js/site.js"></script>
<
<!--End Calender-->
<!---Google Analytics Designmaz.net-->

<style>
    body {font-family: Arial, Helvetica, sans-serif;}
    
    /* The Modal (background) */
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    
    /* Modal Content */
    .modal-content {
      position: relative;
      background-color: #fefefe;
      margin: auto;
      padding: 0;
      border: 1px solid #888;
      width: 80%;
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
      -webkit-animation-name: animatetop;
      -webkit-animation-duration: 0.4s;
      animation-name: animatetop;
      animation-duration: 0.4s
    }
    
    /* Add Animation */
    @-webkit-keyframes animatetop {
      from {top:-300px; opacity:0} 
      to {top:0; opacity:1}
    }
    
    @keyframes animatetop {
      from {top:-300px; opacity:0}
      to {top:0; opacity:1}
    }
    
    /* The Close Button */
    .close {
      color: white;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }
    
    .close:hover,
    .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
    
    .modal-header {
      padding: 2px 16px;
      background-color: #046587;
      color: white;
    }
    
    .modal-body {padding: 2px 16px;}
    
    .modal-footer {
      padding: 2px 16px;
      background-color: #046587;
      color: white;
    }
    </style>

</head>

<body class="grad1">
    <div class="container">
        <div class="row">
            <div class="principal col-10">
            <?php
                include("modulosphp/barra.php");

            ?>
            <form method="post" action="loginIniciarS.php">
        <!--aqui se pone todo lo que tiene la caja negra del formulario-->
                <div class="row justify-content-center">
                    <div class="row justify-content-center">
                        <div class="col-sm-10 col-md-8 col-lg-8 colorTextoSilver text-center tex  colorDivBienvenida m-sm-5 mb-sm-0 p-2 rounded-top sombraForm">
                        <h2 style="font-size: 38px">Citas</h2>
                        </div>
                    </div>
            
                    <div class="row justify-content-center">
                        <div style="margin-bottom: 20px;" class="col-sm-10 col-md-8 col-lg-8 bg-dark text-white mt-sm-0 p-5 pt-4 rounded-bottom sombraForm">

                
                                    <h2>Selecciona un dia en el siguiente calendario para obtener detalle de las citas programadas en dicho dia</h2>

                            <!--main-->
                                    <div class="main" style="margin-left: 0px; margin-right: 0px; width: 100%">  
                                        <div class="calnder">
                                            <div class="column_right_grid calender">
                                                    
                                                <div class="cal1">
                                                    <div class="clndr">
                                                        <div class="clndr-controls" style="border-radius: 8px;">
                                                            <div class="clndr-control-button">
                                                                <p class="clndr-previous-button">previous</p>
                                                            </div>
                                                            <div class="month">SEPTEMBER 2015</div>
                                                            <div class="clndr-control-button rightalign">
                                                                <p class="clndr-next-button">next</p>
                                                            </div>
                                                        </div>
                                                        <table class="clndr-table">
                                                            <thead>
                                                                <tr class="header-days">
                                                                    <td class="header-day">DOM</td>
                                                                    <td class="header-day">MON</td>
                                                                    <td class="header-day">TUE</td>
                                                                    <td class="header-day">WED</td>
                                                                    <td class="header-day">THU</td>
                                                                    <td class="header-day">FRI</td>
                                                                    <td class="header-day">SAT</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class="day past adjacent-month last-month calendar-day-2015-04-26"><div class="day-contents">26</div></td>
                                                                    <td class="day past adjacent-month last-month calendar-day-2015-04-27"><div class="day-contents">27</div></td>
                                                                    <td class="day past adjacent-month last-month calendar-day-2015-04-28"><div class="day-contents">28</div></td>
                                                                    <td class="day past adjacent-month last-month calendar-day-2015-04-29"><div class="day-contents">29</div></td>
                                                                    <td class="day past adjacent-month last-month calendar-day-2015-04-30"><div class="day-contents">30</div></td>
                                                                    <td class="day past calendar-day-2015-05-01"><div class="day-contents">1</div></td>
                                                                    <td class="day past calendar-day-2015-05-02"><div class="day-contents">2</div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="day past calendar-day-2015-05-03"><div class="day-contents">3</div></td>
                                                                    <td class="day past calendar-day-2015-05-04"><div class="day-contents">4</div></td>
                                                                    <td class="day past calendar-day-2015-05-05"><div class="day-contents">5</div></td>
                                                                    <td class="day past calendar-day-2015-05-06"><div class="day-contents">6</div></td>
                                                                    <td class="day past calendar-day-2015-05-07"><div class="day-contents">7</div></td>
                                                                    <td class="day past calendar-day-2015-05-08"><div class="day-contents">8</div></td>
                                                                    <td class="day past calendar-day-2015-05-09"><div class="day-contents">9</div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="day past event calendar-day-2015-05-10"><div class="day-contents">10</div></td>
                                                                    <td class="day past event calendar-day-2015-05-11"><div class="day-contents">11</div></td>
                                                                    <td class="day past event calendar-day-2015-05-12"><div class="day-contents">12</div></td>
                                                                    <td class="day past event calendar-day-2015-05-13"><div class="day-contents">13</div></td>
                                                                    <td class="day today event calendar-day-2015-05-14"><div class="day-contents">14</div></td>
                                                                    <td class="day calendar-day-2015-05-15"><div class="day-contents">15</div></td>
                                                                    <td class="day calendar-day-2015-05-16"><div class="day-contents">16</div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="day calendar-day-2015-05-17"><div class="day-contents">17</div></td>
                                                                    <td class="day calendar-day-2015-05-18"><div class="day-contents">18</div></td>
                                                                    <td class="day calendar-day-2015-05-19"><div class="day-contents">19</div></td>
                                                                    <td class="day calendar-day-2015-05-20"><div class="day-contents">20</div></td>
                                                                    <td class="day event calendar-day-2015-05-21"><div class="day-contents">21</div></td>
                                                                    <td class="day event calendar-day-2015-05-22"><div class="day-contents">22</div></td>
                                                                    <td class="day event calendar-day-2015-05-23"><div class="day-contents">23</div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="day calendar-day-2015-05-24"><div class="day-contents">24</div></td>
                                                                    <td class="day calendar-day-2015-05-25"><div class="day-contents">25</div></td>
                                                                    <td class="day calendar-day-2015-05-26"><div class="day-contents">26</div></td>
                                                                    <td class="day calendar-day-2015-05-27"><div class="day-contents">27</div></td>
                                                                    <td class="day calendar-day-2015-05-28"><div class="day-contents">28</div></td>
                                                                    <td class="day calendar-day-2015-05-29"><div class="day-contents">29</div></td>
                                                                    <td class="day calendar-day-2015-05-30"><div class="day-contents">30</div></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="day calendar-day-2015-05-31"><div class="day-contents">31</div></td>
                                                                    <td class="day adjacent-month next-month calendar-day-2015-06-01"><div class="day-contents">1</div></td>
                                                                    <td class="day adjacent-month next-month calendar-day-2015-06-02"><div class="day-contents">2</div></td>
                                                                    <td class="day adjacent-month next-month calendar-day-2015-06-03"><div class="day-contents">3</div></td>
                                                                    <td class="day adjacent-month next-month calendar-day-2015-06-04"><div class="day-contents">4</div></td>
                                                                    <td class="day adjacent-month next-month calendar-day-2015-06-05"><div class="day-contents">5</div></td>
                                                                    <td class="day adjacent-month next-month calendar-day-2015-06-06"><div class="day-contents">6</div></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//main-->
                        
                        <!-- The Modal -->
                                    <div id="myModal" class="modal">

                                        <!-- Modal content -->
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <span class="close">&times;</span>
                                            <h2 id="tituloModal"></h2>
                                        </div>
                                        <div class="modal-body" style="text-align:left">
                                            <p id="resultado" class="text-black"></p>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                        </div>
                
                                </div>
                                <div id="myModal2" class="modal">

                                        <!-- Modal content -->
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <span class="close2">&times;</span>
                                            <h2 id="tituloModal"></h2>
                                        </div>
                                        <div class="modal-body">
                                            <p id="resultado2" class="text-black"></p>
                                            
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                        </div>
                
                                </div>





                            </div>
                            <br>


                            <br>
                        </div>
                    </div>
            
            


                </div>


            </form>
        </div>
    </div>
    <script src="jsCitas.js"></script>
</body>

</html>