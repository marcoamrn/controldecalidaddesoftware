<?php
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
	//Aqui definiremos que usuarios pueden entrar
	if (isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true) {
		echo "No puedes entrar aqui, ya iniciaste sesion";
	} else {
        echo "Bienvenido a el login!";
		
	}
?>


<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <title>Login</title>
    <!-- LIBRERIAS -->
			<?php include("modulosphp/librerias.php");  ?>
		<!-- FIN DE LIBRERIAS -->
    <script src="https://unpkg.com/boxicons@2.0.9/dist/boxicons.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="jsCitas.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--web-fonts-->
<link href='//fonts.googleapis.com/css?family=Romanesco' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:400,500,100,100italic,300,300italic,500italic,700,700italic,900,900italic,400italic' rel='stylesheet' type='text/css'>
<!--//web-fonts-->
<!--Calender-->
<script src="js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="css/clndr.css" type="text/css" />
<script src="js/underscore-min.js"></script>
<script src= "js/moment-2.2.1.js"></script>
<script src="js/clndr.js"></script>
<script src="js/site.js"></script>
<
<!--End Calender-->
<!---Google Analytics Designmaz.net-->

<style>
    body {font-family: Arial, Helvetica, sans-serif;}
    
    /* The Modal (background) */
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    
    /* Modal Content */
    .modal-content {
      position: relative;
      background-color: #fefefe;
      margin: auto;
      padding: 0;
      border: 1px solid #888;
      width: 80%;
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
      -webkit-animation-name: animatetop;
      -webkit-animation-duration: 0.4s;
      animation-name: animatetop;
      animation-duration: 0.4s
    }
    
    /* Add Animation */
    @-webkit-keyframes animatetop {
      from {top:-300px; opacity:0} 
      to {top:0; opacity:1}
    }
    
    @keyframes animatetop {
      from {top:-300px; opacity:0}
      to {top:0; opacity:1}
    }
    
    /* The Close Button */
    .close {
      color: white;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }
    
    .close:hover,
    .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
    
    .modal-header {
      padding: 2px 16px;
      background-color: #046587;
      color: white;
    }
    
    .modal-body {padding: 2px 16px;}
    
    .modal-footer {
      padding: 2px 16px;
      background-color: #046587;
      color: white;
    }
    </style>

</head>

<body class="grad1">
    <div class="container">
        <div class="row">
            <div class="principal col-10">
            <?php
                include("modulosphp/barra.php");

            ?>
            <form method="post" action="loginIniciarS.php">
        <!--aqui se pone todo lo que tiene la caja negra del formulario-->
                <div class="row justify-content-center">
                    <div class="row justify-content-center">
                        <div class="col-sm-10 col-md-10 col-lg-10 colorTextoSilver text-center tex  colorDivBienvenida m-sm-5 mb-sm-0 p-2 rounded-top sombraForm">
                        <h2 style="font-size: 38px">Citas</h2>
                        </div>
                    </div>
            
                    <div class="row justify-content-center">
                        <div style="margin-bottom: 20px;" class="col-sm-10 col-md-10 col-lg-10 bg-dark text-white mt-sm-0 p-5 pt-4 rounded-bottom sombraForm">

                
                                    <h2>Estas son tus citas programadas</h2>
                                    <?php

                                        include("conexion.php");
                                        $link = conectar();
                                        $queryCitasCliente = "SELECT Fecha, Hora, `idCita`, `idCliente`, `servSelec`, `costoTotal`, `horasDestinadas` FROM `detallecitas` WHERE `idCliente` = " . $_SESSION["idCliente"] ;
                                        $consultaCitasCliente =  mysqli_query($link, $queryCitasCliente);
                                        print('<table class="table table-dark table-striped">');
		                                print('<tr><th>Fecha</th><th>Hora</th><th>Servicios seleccionados</th><th>Precio</th><th>Accion</th>');
                                        while($fila = mysqli_fetch_row($consultaCitasCliente)){
                                            $primerCadena = str_replace("_"," ",$fila[4]);
                                            $segundaCadena = str_replace(";","<br>",$primerCadena);
                                            $cadena_cortada = substr($fila[1], 0, 5);
                                            print("<tr> <td> $fila[0] </td> <td>  $cadena_cortada </td><td> $segundaCadena  </td><td>  $fila[5] </td><td> <button type='button' class='btn btn-danger' value='".$cadena_cortada."' onclick='elimCita(".$fila[2].");'>Eliminar cita</button> </td> ");
                                        }
                                    ?>
                            
                        
                        <!-- The Modal -->
                                    <div id="myModal" class="modal">

                                        <!-- Modal content -->
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <span class="close">&times;</span>
                                            <h2 id="tituloModal">1</h2>
                                        </div>
                                        <div class="modal-body" style="text-align:left">
                                            <p id="resultado" class="text-black"></p>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                        </div>
                
                                </div>
                                <div id="myModal2" class="modal">

                                        <!-- Modal content -->
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <span class="close2">&times;</span>
                                            <h2 id="tituloModal"></h2>
                                        </div>
                                        <div class="modal-body">
                                            <p id="resultado2" class="text-black"></p>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                        </div>
                
                                </div>





                            </div>
                            <br>


                            <br>
                        </div>
                    </div>
            
            


                </div>


            </form>
        </div>
    </div>
    <script src="jsCitas.js"></script>
</body>

</html>