<?php
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
if (isset($_SESSION['sesionIniciada']) && $_SESSION['sesionIniciada'] == true) {
	header('Location: index.php');
} else {
	echo "Bienvenido a el registro!";
	
}



?>


<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
	<!-- LIBRERIAS -->
	<?php include("modulosphp/librerias.php");  ?>
		<!-- FIN DE LIBRERIAS -->
    <title>Advanced Search Form</title>
</head>

<body class="grad1">
	<div class="container">
		<div class="row">
			<div class="principal col-10">
			<?php
				include("modulosphp/barra.php");

			?>
	<form method="post" action="registroForm.php">
    
    	<!--aqui se pone todo lo que tiene la caja negra del formulario-->
    	<div class="row justify-content-sm-center ">
			<div class="col-sm-10 col-md-8 col-lg-6 colorTextoSilver text-center tex  colorDivBienvenida m-sm-5 mb-sm-0 p-2 rounded-top sombraForm">
				<h2>Bienvenido al registro</h2>
			</div>
			
    		<div style="margin-bottom: 20px;" class="col-sm-10 col-md-8 col-lg-6 bg-dark text-white mt-sm-0 p-5 pt-4 rounded-bottom sombraForm">
    			
    			<div class="row">
					
					<label for="first-name" class="col-form-label col-3  mb-sm-3">Nombre/s</label>
    				<div class="col-12 col-sm-9 ">
						<input name="txtBoxNombre" type="text" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="Nombre/s" id="nombres" required>
    				</div>


					<label for="last-name" class="col-form-label col-sm-3  mb-sm-3">Apellidos</label>
    				<div class="col-6 col-sm-4">
                		<input name="txtBoxApellidoPat" type="text" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="Paterno" id="apellidoP" required>
    				</div>
    				<div class="col-6 col-sm-5">
                		<input name="txtBoxApellidoMat" type="text" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="Materno" id="apellidoM" required>
    				</div>

					<label for="number" class="col-form-label col-12 col-sm-5 col-xl-4 mb-sm-3">Numero de celular</label>
    				<div class="col-sm-7 col-xl-8">
                		<input name="txtBoxNumCel" type="text" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="000-000-0000" id="numCel" required>
    				</div>

					<label for="number" class="col-form-label col-12 col-sm-2 mb-sm-3">Email</label>
    				<div class="col-sm-10">
                		<input name="txtBoxCorreo" type="text" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="correo@ejemplo.com" id="numCel" required>
    				</div>
					
					<label for="number" class="col-form-label col-12 col-sm-2 mb-sm-3">Usuario</label>
    				<div class="col-sm-10">
                		<input name="txtBoxNombreUs" type="text" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="Usuario" id="numCel" required>
    				</div>
					
					<label for="number" class="col-form-label col-12 col-sm-3 mb-sm-3">Contraseña</label>
    				<div class="col-sm-9">
                		<input name="txtBoxContraseña" type="password" class="form-control-plaintext colorTextoSilver colorBorderBottomLight" placeholder="**********" id="numCel" required>
    				</div>

    				
    			</div>
    			<br>
    			<div class="row">
    				<div class="d-grid ">
    					<button type="submit" class="btn colorBoton btn-lg btn-responsive" id="search"> <span class="glyphicon glyphicon-search"></span>Registrarse</button>
    				</div>
    				
    			</div>
    			<br>
			
    		</div>
    		
			</form>
    	</div>
</div>
</div>
    </div>

</body>

</html>
