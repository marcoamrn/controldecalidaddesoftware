﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="administrarSucursales.aspx.cs" Inherits="SJActualizado.WebForm5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Sucursal</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    <img src="img/100.png" class="logo-brand" alt="logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.aspx">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Info.aspx">Información</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrar menú
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="admin_agregarProductos.aspx">Modificar</a>
                                <a class="dropdown-item" href="mesero_verMenu.aspx">Mostrar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="administrarPersonal.aspx">Administrar personal
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="administrarSucursal.aspx">Administrar sucursal
                            </a>
                        </li>

                        <li class="nav-item">
                            <asp:Button runat="server" ID="botonCerrrSesion" class="btn btn-dark" OnClick="botonCerrrSesion_Click" Text="Cerrar sesion" formnovalidate></asp:Button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!--Lo importante del formulario aqui empieza-->

        <section class="pt-5">
            <div class="container ">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <h1>Jorobas Beers</h1>
                        <div class="container py-4">
                            <h2>Sucursales</h2>
                            <div class="row">
                                <div class="col mb-3">
                                    <label for="nombre" class="form-label">Nombre</label>
                                    <asp:TextBox runat="server" ID="textboxNombreSuc" type="text" class="form-control" placeholder="Nombre sucursal" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mb-3">
                                    <label for="direccion" class="form-label">Dirección</label>
                                    <asp:TextBox runat="server" ID="textboxDireccionSuc" type="text" class="form-control" placeholder="Dirección" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mb-3">
                                    <label for="Email" class="form-label">Email</label>
                                    <asp:TextBox runat="server" ID="textboxEmail" type="email" class="form-control" placeholder="Email" required></asp:TextBox>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col mb-3">
                                    <label for="telefono" class="form-label">Telefono</label>
                                    <asp:TextBox runat="server" ID="textboxTelefonoSuc" type="text" class="form-control" placeholder="Telefono" required></asp:TextBox>
                                </div>
                                <div class="col">
                                    <label for="cp" class="form-label">C.P.</label>
                                    <asp:TextBox runat="server" ID="textboxCodigoP" type="text" class="form-control" placeholder="C.P." required></asp:TextBox>
                                </div>
                            </div>
                            <div class="row text-center py-4">
                                <div class="col-12 mb-0">
                                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar sucursal" CssClass="btn btn-dark"  OnClick="btnGuardar_Click"></asp:Button>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <img src="img/lugar.jpg" class="img-fluid py-5" alt="">
                    </div>
                </div>
            </div>
        </section>
        <div class="container pb-5">
            <br />
            <center>

                <asp:TextBox ID="idActualizar" runat="server" type="number" class="form-group col-md-2" name="id" AutoPostBack="true" placeholder="ID" OnTextChanged="idActualizar_TextChanged"></asp:TextBox>
            </center>
            <div class="col text-center ">

                <asp:Button ID="btnEliminar" runat="server" Text="Eliminar por ID" CssClass="btn btn-primary" OnClick="btnEliminar_Click" />
                <asp:Button ID="btnActualizar" runat="server" Text="Actualizar por ID" CssClass="btn btn-primary" OnClick="btnActualizar_Click" />

            </div>

        </div>
        <div class="container">
            <div id="cuadroConf" runat="server" class=""></div>
            <asp:GridView ID="listaSucursales" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-dark small">
                <Columns>
                    <asp:BoundField DataField="idSucursal" HeaderText="ID" />
                    <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                    <asp:BoundField DataField="telsucursal" HeaderText="Telefono de Sucursal" />
                    <asp:BoundField DataField="cp" HeaderText="Codigo postal" />
                    <asp:BoundField DataField="calle" HeaderText="Calle" />
                    <asp:BoundField DataField="email" HeaderText="Email" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" Text="Seleccionar" AutoPostBack="true" ID="lnkSelect" CommandArgument='<%# Eval("idSucursal") %>' OnClick="lnkSelect_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>

        <!--Aqui termina-->


    </form>


    <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
        <div class="container text-center">
            <small>Copyright 2020 &copy; Jorobas Beers</small>
        </div>
    </footer>

    <script src="../js/jquery-3.5.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>
