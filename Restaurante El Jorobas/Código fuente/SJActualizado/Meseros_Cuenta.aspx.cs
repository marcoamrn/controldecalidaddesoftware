﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;

namespace SJActualizado
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        readonly ConexionSQL conexionSQL = new ConexionSQL();

        

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Application["personalActivo"] == false || (string)Application["nombreRolPersonalActivo"] != "Mesero")
            {
                //se redirecciona a su pagina de personal o login
                RedirigirAWeb();
            }
            EncenderApagarBtnGenerarVenta();
            ActualizarDatosVenta();
        }

        protected void numUnidades_TextChanged(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();
            int idInput = 0;
            double precio = 0;
            if (idProducto.Text != "")
            {
                idInput = int.Parse(idProducto.Text);
            }
            //buscamos la fila con el id seleccionado
            string querySQLSELECTPID = "SELECT precio FROM producto WHERE idproducto=" + idInput;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            //ejecutamos el query
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };
            //creamos nuestra tabla de lectura y la llenamos con la informacion sql
            DataTable table = new DataTable();
            adapter.Fill(table);
            //En el caso de que nuestra tabla tenga informacion, llenamos nuestros campos con sus datos

            if (table.Rows.Count > 0)
            {
               
                precio = double.Parse(table.Rows[0][0].ToString());
                
            }
            //si no hay informacion, vaciamos nuestros textbox
            else
            {
                productoTexto.InnerText = "";

            }
            if (numUnidades.Text != "")
            {
                if (int.Parse(numUnidades.Text) > 0)
                {
                    gastoTexto.InnerText = "$" + (precio * double.Parse(numUnidades.Text)).ToString();
                }
                else
                {
                    gastoTexto.InnerText = "$0";
                }
            }
            
            actualizarCambio();
        }


        protected void idProducto_TextChanged(object sender, EventArgs e)
        {

            conexionSQL.IntentarConexion();

            int idInput = 0;
            if (idProducto.Text != "")
            {
                idInput = int.Parse(idProducto.Text);
            }

            //buscamos la fila con el id seleccionado
            string querySQLSELECTPID = "SELECT nombre, precio FROM producto WHERE idproducto=" + idInput;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            //ejecutamos el query
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };
            //creamos nuestra tabla de lectura y la llenamos con la informacion sql
            DataTable table = new DataTable();
            adapter.Fill(table);
            //En el caso de que nuestra tabla tenga informacion, llenamos nuestros campos con sus datos

            if (table.Rows.Count > 0)
            {
                productoTexto.InnerText = table.Rows[0][0].ToString();
                float precio = float.Parse(table.Rows[0][1].ToString());
                gastoTexto.InnerText = "$"+ precio.ToString();
                numUnidades.Text = "1";


            }
            //si no hay informacion, vaciamos nuestros textbox
            else
            {
                productoTexto.InnerText = "";
                gastoTexto.InnerText = "";
                numUnidades.Text = "";

            }
            actualizarCambio();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            //la tabla se tuvo que crear en global, para no ser reasignada cada que recarga la pagina
            //se añade un producto a la tabla general
            if(productoTexto.InnerText != "" && Int32.Parse(gastoTexto.InnerText.Remove(0, 1)) > 0)
            {
                Global.TablaPreOrden.Rows.Add(new object[] { idProducto.Text, productoTexto.InnerText, numUnidades.Text, Int32.Parse(gastoTexto.InnerText.Remove(0, 1)) });
            }
            else
            {
                //insertar mensaje de error en caso de no existir el producto
            }
            
            ActualizarDatosVenta();
            //vaciamos todos los campos
            idProducto.Text = "";
            numUnidades.Text = "";
            productoTexto.InnerText = "";
            gastoTexto.InnerText = "";
            EncenderApagarBtnGenerarVenta();







            /*conexionSQL.IntentarConexion();
            string queryCrearOrden = "INSERT INTO orden (fecha, idcliente, idpersonal, idSucursal) VALUES (CURRENT_TIMESTAMP, NULL, " + (int)Application["idPersonalActivo"] + ", " + Application["idSucursal"] + ");";
            MySqlCommand comandoInsertar = new MySqlCommand(queryCrearOrden, conexionSQL.conex);
            //se ejecuta el query y se registra el numero de columnas modificadas
            int numFilasMod = comandoInsertar.ExecuteNonQuery();
            conexionSQL.IntentarCerrarConexion();*/

        }

        private void ActualizarDatosVenta()
        {
            //se actualiza
            listaCuenta.DataSource = Global.TablaPreOrden;
            listaCuenta.DataBind();
            //se hace la suma de los precios que hay en la tabla
            float cuentaTotal = 0;
            foreach (DataRow dataRow in Global.TablaPreOrden.Rows)
            {
                cuentaTotal = cuentaTotal + float.Parse(dataRow["precioTotalProductos"].ToString());
            }
            acumulado.InnerText = cuentaTotal.ToString();
            actualizarCambio();
        }

        public static DataTable CrearColumnas()
        {
            DataTable tablaCol = new DataTable();
            tablaCol.Columns.Add("idProducto", typeof(int));
            tablaCol.Columns.Add("nombreProducto", typeof(string));
            tablaCol.Columns.Add("cantidadProducto", typeof(int));
            tablaCol.Columns.Add("precioTotalProductos", typeof(int));
            return tablaCol;
        }


        void RedirigirAWeb()
        {
            if ((bool)Application["personalActivo"] == false)
            {
                Response.Redirect("login.aspx");
            }
            if ((string)Application["nombreRolPersonalActivo"] == "Administrador")
            {
                //redirigir a la pagina de administrador
                Response.Redirect("Pagina_Administrador.aspx");

            }
            else if ((string)Application["nombreRolPersonalActivo"] == "Mesero")
            {
                //redirigir a la pagina de meseros
                Response.Redirect("Pagina_Meseros.aspx");
            }
        }

        protected void botonCerrrSesion_Click(object sender, EventArgs e)
        {
            CerrarSesion();
        }
        public void CerrarSesion()
        {
            Application["personalActivo"] = false;
            Application["nombreRolPersonalActivo"] = "";
            Application["idPersonalActivo"] = 0;
            Response.Redirect("index.aspx");
        }

        protected void presupuestoC_TextChanged(object sender, EventArgs e)
        {
            actualizarCambio();
        }
        void actualizarCambio()
        {
            if (presupuestoC.Text != "")
            {
                if (int.Parse(presupuestoC.Text) >= int.Parse(acumulado.InnerText))
                {
                    if (int.Parse(acumulado.InnerText) > 0)
                    {
                        textoCambio.InnerText = (int.Parse(presupuestoC.Text) - int.Parse(acumulado.InnerText)).ToString();
                        textoCambio.Attributes.Add("class", "text-dark");
                    }
                    else
                    {
                        textoCambio.InnerText = "Esperando productos...";
                        textoCambio.Attributes.Add("class", "text-warning");
                    }
                    

                }
                else
                {
                    
                    textoCambio.InnerText = "No se puede efectuar la venta!";
                    textoCambio.Attributes.Add("class", "text-danger");
                }
            }
            else
            {
                
                textoCambio.InnerText = "Esperando datos...";
                textoCambio.Attributes.Add("class", "text-warning");
            }
                
                
                
        } 
        void EncenderApagarBtnGenerarVenta()
        {
            if(presupuestoC.Text != "")
            {
                if (int.Parse(presupuestoC.Text) > 0 && int.Parse(acumulado.InnerText) > 0)
                {
                    if (int.Parse(presupuestoC.Text) >= int.Parse(acumulado.InnerText))
                    {
                        efectuarVenta.Enabled = true;
                    }
                    else
                    {
                        efectuarVenta.Enabled = false;

                    }
                }
                else
                {
                    efectuarVenta.Enabled = false;

                }

            }
            else
            {
                efectuarVenta.Enabled = false;
                
            }

        }

        protected void efectuarVenta_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();
            //Generamos una orden en mysql
            string querySQLNuevaOrden = "INSERT INTO orden(totalorden, idpersonal, idsucursal) VALUES ( " + acumulado.InnerText + "  , " + Application["idPersonalActivo"].ToString() + "," + Application["idSucursal"].ToString() + "); SELECT last_insert_id();";
            //indicamos a mySQL el query que queremos ejecutar con la conexion previamente abierta
            MySqlCommand comandoNOrden = new MySqlCommand(querySQLNuevaOrden, conexionSQL.conex);
            MySqlDataAdapter Adapter = new MySqlDataAdapter(comandoNOrden);
            DataTable idOrdenGenerada = new DataTable();
            Adapter.Fill(idOrdenGenerada);
            string idOrdenActual = idOrdenGenerada.Rows[0][0].ToString();

            foreach(DataRow fila in Global.TablaPreOrden.Rows)
            {
                string querySQLDetalleOrden = "INSERT INTO detalle(idorden, idproducto, cantidadv, preciov) VALUES ("+idOrdenActual+", " + fila[0] + ", "+fila[2]+", "+fila[3]+")";
                MySqlCommand mySqlCommand = new MySqlCommand(querySQLDetalleOrden, conexionSQL.conex);
                mySqlCommand.ExecuteNonQuery();
            }
            Global.TablaPreOrden.Clear();
            ActualizarDatosVenta();

            presupuestoC.Text = "";



            EncenderApagarBtnGenerarVenta();

            conexionSQL.IntentarCerrarConexion();
        }
    }
}