﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

namespace SJActualizado
{
    public partial class pagina_meseros : System.Web.UI.Page
    {
        readonly ConexionSQL conexionSQL = new ConexionSQL();
        protected void Page_Load(object sender, EventArgs e)
        {

            if ((bool)Application["personalActivo"] == false || (string)Application["nombreRolPersonalActivo"] != "Mesero")
            {
                //se redirecciona a su pagina de personal o login
                RedirigirAWeb();
            }
            conexionSQL.IntentarConexion();
            string querysql = "SELECT nombre from personal where idpersonal=" + (string)Application["idPersonalActivo"] + ";";
            //indicamos a mySQL el query que queremos ejecutar con la conexion previamente abierta
            MySqlCommand comando = new MySqlCommand(querysql, conexionSQL.conex);
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };
            //creamos nuestra tabla de lectura y la llenamos
            DataTable table = new DataTable();
            adapter.Fill(table);


            conexionSQL.IntentarCerrarConexion();
            mensajeBienvenida.InnerText = "Bienvenido Mesero: " + table.Rows[0][0].ToString();
        }

        void RedirigirAWeb()
        {
            if ((bool)Application["personalActivo"] == false)
            {
                Response.Redirect("login.aspx");
            }
            if ((string)Application["nombreRolPersonalActivo"] == "Administrador")
            {
                //redirigir a la pagina de administrador
                Response.Redirect("Pagina_Administrador.aspx");

            }
            else if ((string)Application["nombreRolPersonalActivo"] == "Mesero")
            {
                //redirigir a la pagina de meseros
                Response.Redirect("Pagina_Meseros.aspx");
            }
        }

        protected void botonCerrrSesion_Click(object sender, EventArgs e)
        {
            CerrarSesion();
        }
        public void CerrarSesion()
        {
            Application["personalActivo"] = false;
            Application["nombreRolPersonalActivo"] = "";
            Application["idPersonalActivo"] = 0;
            Response.Redirect("index.aspx");
        }
    }
}