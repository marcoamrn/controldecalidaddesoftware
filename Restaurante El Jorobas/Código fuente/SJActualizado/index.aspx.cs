﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SJActualizado
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Application["personalActivo"] == true)
            {
                iniciarSesion.Text = "Ir a pagina administracion";
            }
            else
            {
                iniciarSesion.Text = "Iniciar sesion";
            }
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {

        }

        protected void iniciarSesion_Click(object sender, EventArgs e)
        {
            Response.Redirect("login.aspx");
        }
    }
}