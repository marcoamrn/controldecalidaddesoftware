﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

namespace SJActualizado
{
    public partial class WebForm6 : System.Web.UI.Page
    {
        //creamos la conexion con el servidor
        readonly ConexionSQL conexionSQL = new ConexionSQL();
        readonly string querySQLSelectTablaProductos = "SELECT `idpersonal`, `nombre`, `paterno`, `materno`, `correo`, `telefono`, DATE_FORMAT(fechanac,'%d/%m/%Y') AS fechanac, `idjefe`, `contrasena`, `idSucursal`, `Tipo_empleado`, `salario` FROM `personal`;";
        bool hayIdValido = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Application["personalActivo"] == false || (string)Application["nombreRolPersonalActivo"] != "Administrador")
            {
                //se redirecciona a su pagina de personal
                RedirigirAWeb();
            }
            conexionSQL.IntentarConexion();

            //rellenamos nuestra tabla 
            RellenarTablaQuery(listaProductos, querySQLSelectTablaProductos, conexionSQL);

            //dejamos nuestra alerta vacia
            cuadroConf.InnerText = "";

            //deshabilitamos nuestros botones
            btnActualizar.Enabled = false;
            btnEliminar.Enabled = false;

            rellenaridJefe();

            rellenarSucursal();
            







            //cerramos nuestra conexion
            conexionSQL.IntentarCerrarConexion();
        }

        protected void btnAgregarEmpleado_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();

            //leemos los textboxes y los guardamos en variables Nota los nombres que tienen "_" al final son bariables c#
            string textboxNombreEmpleado_ = textboxNombreEmpleado.Text;
            string textboxApellidoPat_ = textboxApellidoPat.Text;
            string textboxApellidoMat_ = textboxApellidoMat.Text;
            string textboxFechaNac_ = textboxFechaNac.Text;
            string textboxTelefono_ = textboxTelefono.Text;
            string textboxCorreo_ = textboxCorreo.Text;
            string SelectIdJefe_ = SelectIdJefe.Value;
            string SelectSucursal_ = SelectSucursal.Value;
            string selectTipoEmpleado_ = selectTipoEmpleado.Value;
            string textboxSalario_ = textboxSalario.Text;
            string textboxContrasena_ = textboxContrasena.Text;
            string textboxValidarContrasena_ = textboxValidarContrasena.Text;

            cuadroConf.InnerText = textboxFechaNac_;

            if (textboxContrasena_ == textboxValidarContrasena_)
            {

                string queryInsertarEmpleado = "INSERT INTO personal(nombre, paterno, materno, correo, telefono, fechanac, idjefe, contrasena, idSucursal, Tipo_empleado, salario)" +
                    " VALUES ('" + textboxNombreEmpleado_ + "','" + textboxApellidoPat_ + "','" + textboxApellidoMat_ + "','" + textboxCorreo_ + "','" + textboxTelefono_ + "','" + textboxFechaNac_ + "' ," + SelectIdJefe_ + " ,'" + textboxContrasena_ + "' ," + SelectSucursal_ + " ,'" + selectTipoEmpleado_ + "' ," + textboxSalario_ + " )";
                //indicamos a mySQL el query que queremos ejecutar con la conexion previamente abierta
                MySqlCommand comandoInsertar = new MySqlCommand(queryInsertarEmpleado, conexionSQL.conex);
                //se ejecuta el query y se registra el numero de columnas modificadas
                int numFilasMod = comandoInsertar.ExecuteNonQuery();
                //verificamos el numero de columnas modificadas para mostrar mensajes
                if (numFilasMod < 1)
                {
                    cuadroConf.InnerText = "Ha ocurrido un error";
                    cuadroConf.Attributes.Add("class", "alert alert-danger");
                }
                else
                {
                    cuadroConf.InnerText = "Listo, elemento agregado";
                    cuadroConf.Attributes.Add("class", "alert alert-success");
                }
                //actualizamos nuestra tabla
                RellenarTablaQuery(listaProductos, querySQLSelectTablaProductos, conexionSQL);

                //vaciamos los elementos de las text box
                VaciarTextboxes();

                //Final del script del boton
                conexionSQL.IntentarCerrarConexion();
            }
            else
            {
                cuadroConf.InnerText = "Las contraseñas no coinciden, por favor vuelve a verificarlas";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }


        }

        protected void idActualizar_TextChanged(object sender, EventArgs e)
        {

            //deshabilitamos el cuadro de eventos
            cuadroConf.Attributes.Add("class", "");
            //asignamos el valor de id que escogimos
            int personalID = 0;
            if (idActualizar.Text != "")
            { personalID = int.Parse(idActualizar.Text); }
            //buscamos la fila con el id seleccionado
            string querySQLSELECTPID = "SELECT `nombre`, `paterno`, `materno`, `correo`, `telefono`, DATE_FORMAT(fechanac,'%d/%m/%Y') AS fechanac, `idjefe`, `contrasena`, `idSucursal`, `Tipo_empleado`, `salario` FROM `personal` WHERE idpersonal=" + personalID;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            //ejecutamos el query
            MySqlDataAdapter adapter = new MySqlDataAdapter { SelectCommand = comando };
            //creamos nuestra tabla de lectura y la llenamos con la informacion sql
            DataTable table = new DataTable();
            adapter.Fill(table);
            //En el caso de que nuestra tabla tenga informacion, llenamos nuestros campos con sus datos
            if (table.Rows.Count > 0)
            {
                int tipoEmpleadoF = 0;
                //asignemos un valo a el tipo de empleado
                if (table.Rows[0][9].ToString() == "Administrador")
                {
                    tipoEmpleadoF = 1;
                }
                else if (table.Rows[0][9].ToString() == "Mesero")
                {
                    tipoEmpleadoF = 2;
                }
                textboxNombreEmpleado.Text = table.Rows[0][0].ToString();
                textboxApellidoPat.Text = table.Rows[0][1].ToString();
                textboxApellidoMat.Text = table.Rows[0][2].ToString();
                textboxFechaNac.Text = table.Rows[0][5].ToString();
                textboxTelefono.Text = table.Rows[0][4].ToString();
                textboxCorreo.Text = table.Rows[0][3].ToString();
                bool idJefeNull = false;
                object valorIDJefe = table.Rows[0][6];
                if (valorIDJefe == DBNull.Value)
                {
                    idJefeNull = true;
                }

                if (idJefeNull)
                {
                    SelectIdJefe.SelectedIndex = 0;
                }
                else
                {
                    SelectIdJefe.DataValueField = table.Rows[0][6].ToString();
                }
                SelectSucursal.SelectedIndex = int.Parse(table.Rows[0][8].ToString()) - 1;
                selectTipoEmpleado.SelectedIndex = tipoEmpleadoF;
                textboxSalario.Text = table.Rows[0][10].ToString();
                hayIdValido = true;
            }
            //si no hay informacion, vaciamos nuestros textbox
            else
            {
                VaciarTextboxes();
                hayIdValido = false;
            }
            //aqui activamos o desactivamos los botones act y eliminar
            if (hayIdValido)
            {
                btnActualizar.Enabled = true;
                btnEliminar.Enabled = true;
            }
            else
            {
                btnActualizar.Enabled = false;
                btnEliminar.Enabled = false;
            }

        }
        protected void habilitarBtns(object sender, EventArgs e)
        {
            //deshabilitamos el cuadro de eventos
            cuadroConf.Attributes.Add("class", "");
            if (hayIdValido)
            {
                btnActualizar.Enabled = true;
                btnEliminar.Enabled = true;
            }
            else
            {
                btnActualizar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();
            string idActualizar_ = idActualizar.Text;

            //leemos los textboxes y los guardamos en variables
            string textboxNombreEmpleado_ = textboxNombreEmpleado.Text;
            string textboxApellidoPat_ = textboxApellidoPat.Text;
            string textboxApellidoMat_ = textboxApellidoMat.Text;

            string textboxContrasena_ = textboxContrasena.Text;
            string textboxValidarContrasena_ = textboxValidarContrasena.Text;
            string contrasenadequery = "";

            string querySQLSELECTPID = "select contrasena from personal WHERE idpersonal= " + idActualizar_;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            //ejecutamos el query
            MySqlDataAdapter adapter = new MySqlDataAdapter { SelectCommand = comando };
            //creamos nuestra tabla de lectura y la llenamos con la informacion sql
            DataTable table = new DataTable();
            adapter.Fill(table);
            //En el caso de que nuestra tabla tenga informacion, llenamos nuestros campos con sus datos
            if (table.Rows.Count > 0)
            {
                contrasenadequery = table.Rows[0][0].ToString();
            }
            

            
                
            string queryDeleteEmpleado = "DELETE FROM personal WHERE (idpersonal=" + idActualizar_ + ")";
            MySqlCommand comandoDelete = new MySqlCommand(queryDeleteEmpleado, conexionSQL.conex);
            //se ejecuta el query y se registra el numero de columnas modificadas



            int numColMod = comandoDelete.ExecuteNonQuery();

            //verificamos el numero de columnas modificadas para mostrar mensajes
            if (numColMod < 1)
            {
                cuadroConf.InnerText = "Ha ocurrido un error: posiblemente el empleado que escribiste no existe, verifica los campos";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }
            else
            {
                cuadroConf.InnerText = "Listo, " + numColMod + " elemento/s borrado/s";
                cuadroConf.Attributes.Add("class", "alert alert-success");
            }
                
                
            
           
               

            
            


            //actualizamos nuestra tabla
            RellenarTablaQuery(listaProductos, querySQLSelectTablaProductos, conexionSQL);

            //vaciamos los elementos de las text box
            VaciarTextboxes();

            conexionSQL.IntentarCerrarConexion();
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();


            //leemos los textboxes y los guardamos en variables Nota los nombres que tienen "_" al final son bariables c#
            string textboxNombreEmpleado_ = textboxNombreEmpleado.Text;
            string textboxApellidoPat_ = textboxApellidoPat.Text;
            string textboxApellidoMat_ = textboxApellidoMat.Text;
            string textboxFechaNac_ = textboxFechaNac.Text;
            string textboxTelefono_ = textboxTelefono.Text;
            string textboxCorreo_ = textboxCorreo.Text;
            string SelectIdJefe_ = SelectIdJefe.Value;
            string SelectSucursal_ = SelectSucursal.Value;
            string selectTipoEmpleado_ = selectTipoEmpleado.Value;
            string textboxSalario_ = textboxSalario.Text;
            string textboxContrasena_ = textboxContrasena.Text;
            string textboxValidarContrasena_ = textboxValidarContrasena.Text;
            if (textboxContrasena_ == textboxValidarContrasena_)
            {
                if (textboxFechaNac_ != "") {
                    string queryActualizarEmpleado = "update personal set nombre='" + textboxNombreEmpleado_ + "', paterno='" + textboxApellidoPat_ + "', materno='" + textboxApellidoMat_ + "', correo='" + textboxCorreo_ + "', telefono= '" + textboxTelefono_ + "', fechanac='" + textboxFechaNac_ + "', idjefe=" + SelectIdJefe_ + ", contrasena='" + textboxContrasena_ + "', idSucursal=" + SelectSucursal_ + ", Tipo_empleado='" + selectTipoEmpleado_ + "', salario=" + textboxSalario_ + " where idpersonal=" + idActualizar.Text;

                    MySqlCommand comandoActualizar = new MySqlCommand(queryActualizarEmpleado, conexionSQL.conex);
                    //se ejecuta el query y se registra el numero de columnas modificadas
                    int numColMod = comandoActualizar.ExecuteNonQuery();

                    //verificamos el numero de columnas modificadas para mostrar mensajes
                    if (numColMod < 1)
                    {
                        cuadroConf.InnerText = "Ha ocurrido un error: posiblemente el elemento que escribiste no existe, verifica los campos";
                        cuadroConf.Attributes.Add("class", "alert alert-danger");
                    }
                    else
                    {
                        cuadroConf.InnerText = "Listo, " + numColMod + " Elemento/s Actualizados/s";
                        cuadroConf.Attributes.Add("class", "alert alert-success");
                    }

                    //actualizamos nuestra tabla
                    RellenarTablaQuery(listaProductos, querySQLSelectTablaProductos, conexionSQL);

                    //vaciamos los elementos de las text box
                    VaciarTextboxes();

                    conexionSQL.IntentarCerrarConexion();
                }
                else
                {
                    cuadroConf.InnerText = "Selecciona una fecha de nacimiento valida, por favor vuelve a verificar los campos";
                    cuadroConf.Attributes.Add("class", "alert alert-danger");
                }
            }
            else
            {
                cuadroConf.InnerText = "Las contraseñas no coinciden, por favor vuelve a verificarlas";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }
        }

        void rellenarSucursal()
        {
            //rellenar select sucursal
            string leerSucursales = "SELECT idSucursal, nombre from sucursal";
            MySqlCommand comandoLeerSucursales = new MySql.Data.MySqlClient.MySqlCommand(leerSucursales, conexionSQL.conex);
            MySqlDataAdapter mySqlDataAdapter1 = new MySql.Data.MySqlClient.MySqlDataAdapter(comandoLeerSucursales);
            DataSet dataSet1 = new DataSet();
            mySqlDataAdapter1.Fill(dataSet1, "idSucursal");
            SelectSucursal.DataSource = dataSet1;
            SelectSucursal.DataTextField = "nombre";
            SelectSucursal.DataValueField = "idSucursal";
            SelectSucursal.DataBind();
        }
        void rellenaridJefe()
        {
            //rellenar select id empleado 
            string leerPersonal = "SELECT * from personal where Tipo_Empleado='Administrador' ";
            MySql.Data.MySqlClient.MySqlCommand comandoLeerPersonal = new MySql.Data.MySqlClient.MySqlCommand(leerPersonal, conexionSQL.conex);
            MySql.Data.MySqlClient.MySqlDataAdapter mySqlDataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter(comandoLeerPersonal);
            DataSet dataSet = new DataSet();
            mySqlDataAdapter.Fill(dataSet, "idpersonal");
            SelectIdJefe.DataSource = dataSet;
            SelectIdJefe.DataTextField = "idpersonal";
            SelectIdJefe.DataValueField = "idpersonal";
            SelectIdJefe.DataBind();
        }
        void RellenarTablaQuery(GridView tablaARellenar, string querySQL, ConexionSQL conexion)
        {
            //ejecutamos nuestro query 
            MySqlCommand comando = new MySqlCommand(querySQL, conexion.conex);
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };

            //creamos nuestra tabla de lectura
            DataTable table = new DataTable();
            adapter.Fill(table);

            //rellenamos nuestra tabla
            tablaARellenar.DataSource = table;
            tablaARellenar.DataBind();

        }

        public void VaciarTextboxes()
        {
            textboxNombreEmpleado.Text = "";
            textboxApellidoPat.Text = "";
            textboxApellidoMat.Text = "";
            textboxFechaNac.Text = "";
            textboxTelefono.Text = "";
            textboxCorreo.Text = "";
            SelectIdJefe.SelectedIndex = 0;
            SelectSucursal.SelectedIndex = 0;
            selectTipoEmpleado.SelectedIndex = 0;
            textboxSalario.Text = "";
            textboxContrasena.Text = "";
            textboxValidarContrasena.Text = "";
            idActualizar.Text = "";
        }

        public void RedirigirAWeb()
        {
            if ((bool)Application["personalActivo"] == false)
            {
                Response.Redirect("login.aspx");
            }
            if ((string)Application["nombreRolPersonalActivo"] == "Administrador")
            {
                //redirigir a la pagina de administrador
                Response.Redirect("Pagina_Administrador.aspx");

            }
            else if ((string)Application["nombreRolPersonalActivo"] == "Mesero")
            {
                //redirigir a la pagina de meseros
                Response.Redirect("Pagina_Meseros.aspx");
            }

        }

        protected void textboxNombreEmpleado_TextChanged(object sender, EventArgs e)
        {
            //deshabilitamos el cuadro de eventos
            cuadroConf.Attributes.Add("class", "");
            //asignamos el valor de id que escogimos
            int personalID = 0;
            if (idActualizar.Text != "")
            { personalID = int.Parse(idActualizar.Text); }
            //buscamos la fila con el id seleccionado
            string querySQLSELECTPID = "SELECT `nombre`, `paterno`, `materno`, `correo`, `telefono`, DATE_FORMAT(fechanac,'%d/%m/%Y') AS fechanac, `idjefe`, `contrasena`, `idSucursal`, `Tipo_empleado`, `salario` FROM `personal` WHERE idpersonal=" + personalID;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            //ejecutamos el query
            MySqlDataAdapter adapter = new MySqlDataAdapter { SelectCommand = comando };
            //creamos nuestra tabla de lectura y la llenamos con la informacion sql
            DataTable table = new DataTable();
            adapter.Fill(table);
            //En el caso de que nuestra tabla tenga informacion, llenamos nuestros campos con sus datos
            if (table.Rows.Count > 0)
            {
                hayIdValido = true;
            }
            else
            {
                hayIdValido = false;
            }
            if (hayIdValido)
            {
                btnActualizar.Enabled = true;
                btnEliminar.Enabled = true;
            }
            else
            {
                btnActualizar.Enabled = false;
                btnEliminar.Enabled = false;
            }
        }
        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            //asignamos el id devuelto por la etiqueta select
            int personalID = Convert.ToInt32((sender as LinkButton).CommandArgument);

            //mostramos un mensaje sobre el id que seleccionaron
            cuadroConf.InnerText = "Seleccionaste el producto con ID: " + personalID.ToString();
            cuadroConf.Attributes.Add("class", "alert alert-info");

            //asignamos y ejecutamos el query
            string querySQLSELECTPID = "SELECT `nombre`, `paterno`, `materno`, `correo`, `telefono`, DATE_FORMAT(fechanac,'%d/%m/%Y') AS fechanac, `idjefe`, `contrasena`, `idSucursal`, `Tipo_empleado`, `salario` FROM `personal` WHERE idpersonal=" + personalID;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };
            //creamos nuestra tabla de lectura y la llenamos
            DataTable table = new DataTable();
            adapter.Fill(table);

            //asignamos los valores de la consulta en sus respectivos campos
            int tipoEmpleadoF = 0;
            //asignemos un valo a el tipo de empleado
            if (table.Rows[0][9].ToString() == "Administrador")
            {
                tipoEmpleadoF = 1;
            }
            else if (table.Rows[0][9].ToString() == "Mesero")
            {
                tipoEmpleadoF = 2;
            }
            textboxNombreEmpleado.Text = table.Rows[0][0].ToString();
            textboxApellidoPat.Text = table.Rows[0][1].ToString();
            textboxApellidoMat.Text = table.Rows[0][2].ToString();
            textboxFechaNac.Text = table.Rows[0][5].ToString();
            textboxTelefono.Text = table.Rows[0][4].ToString();
            textboxCorreo.Text = table.Rows[0][3].ToString();
            bool idJefeNull = false;
            object valorIDJefe = table.Rows[0][6];
            if (valorIDJefe == DBNull.Value)
            {
                idJefeNull = true;
            }

            if (idJefeNull)
            {
                SelectIdJefe.SelectedIndex = 0;
            }
            else
            {
                SelectIdJefe.DataValueField = personalID.ToString();
            }
            SelectSucursal.SelectedIndex = int.Parse(table.Rows[0][8].ToString()) - 1;
            selectTipoEmpleado.SelectedIndex = tipoEmpleadoF;
            textboxSalario.Text = table.Rows[0][10].ToString();

            //encendemos los botones de actualizar y eliminar
            btnActualizar.Enabled = true;
            btnEliminar.Enabled = true;

        }
    }
}