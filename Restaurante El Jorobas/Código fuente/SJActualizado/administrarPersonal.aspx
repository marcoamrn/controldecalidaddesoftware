﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="administrarPersonal.aspx.cs" Inherits="SJActualizado.WebForm6" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Personal</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Sulphur+Point:700&display=swap" rel="stylesheet">
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/grid.css">

</head>

<body class="py-4" data-new-gr-c-s-check-loaded="14.994.0" data-gr-ext-installed="" cz-shortcut-listen="true">
    <form id="form1" runat="server">
        <div>
            <nav class="navbar navbar-expand-lg navbar-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="index.html">
                        <img src="img/100.png" class="logo-brand" alt="logo">

                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
        
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="index.aspx">Inicio</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="Info.aspx">Información</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrar menú
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="admin_agregarProductos.aspx">Modificar</a>
                                    <a class="dropdown-item" href="mesero_verMenu.aspx">Mostrar</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="administrarPersonal.aspx">Administrar personal
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="administrarSucursales.aspx">Administrar sucursal
                                </a>
                            </li>

                            <li class="nav-item">
                                <asp:Button runat="server" ID="botonCerrrSesion" class="btn btn-dark" Text="Cerrar sesion" formnovalidate></asp:Button>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <section class="margTop">

                <div class="container-fluid justify-content-center py-5 marL">


                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <h1>Jorobas Beers</h1>

                            <div class="container py-4">
                                <div class="row">
                                    <div class="col mb-3">
                                        <label for="textboxNombreEmpleado" class="form-label">Nombre</label>
                                        <asp:TextBox runat="server" required="" ID="textboxNombreEmpleado" type="text" class="form-control" placeholder="Nombre" AutoPostBack="true" OnTextChanged="textboxNombreEmpleado_TextChanged"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <label for="textboxApellidoPat" class="form-label">Apellido</label>
                                        <asp:TextBox runat="server" required="" ID="textboxApellidoPat" type="text" class="form-control" placeholder="Paterno" OnTextChanged="textboxNombreEmpleado_TextChanged"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <label for="textboxApellidoMat" class="form-label">Apellido</label>
                                        <asp:TextBox runat="server" required="" ID="textboxApellidoMat" type="text" class="form-control" placeholder="Materno" OnTextChanged="textboxNombreEmpleado_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col mb-3">
                                        <label for="textboxFechaNac" class="form-label">Fecha</label>
                                        <asp:TextBox runat="server" ID="textboxFechaNac" type="date" class="form-control" placeholder="Fecha" OnTextChanged="textboxNombreEmpleado_TextChanged"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <label for="apellido" class="form-label">Telefono</label>
                                        <asp:TextBox runat="server" required="" ID="textboxTelefono" type="tel" pattern="[0-9]{10}" class="form-control" placeholder="Telefono" OnTextChanged="textboxNombreEmpleado_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="textboxCorreo" class="form-label">Correo</label>
                                    <asp:TextBox runat="server" required="" ID="textboxCorreo" type="email" class="form-control" aria-describedby="emailHelp" placeholder="Correo" OnTextChanged="textboxNombreEmpleado_TextChanged"></asp:TextBox>
                                </div>
                                <div class="row">
                                    <div class="col mb-3">

                                        <label for="Jefe" class="form-label">ID Jefe</label>

                                        <select runat="server" class="form-control" name="" id="SelectIdJefe" placeholder="-Selecciona un elemento-">
                                        </select>

                                    </div>
                                    <div class="col mb-3">
                                        <label for="SelectSucursal" class="form-label">Sucursal</label>
                                        <select runat="server" class="form-control" name="" id="SelectSucursal" placeholder="-Selecciona un elemento-" onselect="asignarValSelect">
                                            <option selected="selected" value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col mb-3">
                                        <label for="selectTipoEmpleado" class="form-label">Tipo Empleado</label>
                                        <select runat="server" class="form-control" name="" id="selectTipoEmpleado" placeholder="-Selecciona un elemento-" onselect="asignarValSelect">
                                            <option selected>Selecciona</option>
                                            <option value="Administrador">Administrador</option>
                                            <option value="Mesero">Mesero</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <label for="textboxSalario" class="form-label">Salario</label>
                                        <asp:TextBox runat="server" required="" ID="textboxSalario" type="number" class="form-control" placeholder="$" OnTextChanged="textboxNombreEmpleado_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col mb-3">
                                        <label for="textboxContrasena" class="form-label">Contraseña</label>
                                        <asp:TextBox runat="server" required="" ID="textboxContrasena" type="password" class="form-control" placeholder="********" OnTextChanged="textboxNombreEmpleado_TextChanged"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <label for="textboxValidarContrasena" class="form-label">Ingresa contraseña nuevamente</label>
                                        <asp:TextBox runat="server" required="" ID="textboxValidarContrasena" type="password" class="form-control" placeholder="********" OnTextChanged="habilitarBtns"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="col text-center ">
                                    <asp:Button ID="btnAgregarEmpleado" runat="server" Text="Agregar empleado" CssClass="btn btn-primary" OnClick="btnAgregarEmpleado_Click" />
                                </div>
                                <!--
            <div class="row text-center py-4">
            <div class="col-12 ">
            <button class="btn btn-dark" type="submit">Guardar</button>
            </div>
            </div>
            -->
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <img src="img/personal.jpg" class="img-fluid py-5" alt="">
                        </div>
                    </div>

                    <div class="container pb-5">

                        <br />
                        <center>

                            <asp:TextBox ID="idActualizar" runat="server" type="number" class="form-group col-md-2" name="id" AutoPostBack="true" placeholder="ID" OnTextChanged="idActualizar_TextChanged"></asp:TextBox>
                        </center>
                        <div class="col text-center ">

                            <asp:Button ID="btnEliminar" formnovalidate runat="server" Text="Eliminar por ID" CssClass="btn btn-primary" OnClick="btnEliminar_Click" />
                            <asp:Button ID="btnActualizar" runat="server" Text="Actualizar por ID" CssClass="btn btn-primary" OnClick="btnActualizar_Click" />

                        </div>

                    </div>
                    <div>
                        <div id="cuadroConf" runat="server" class=""></div>
                        <asp:GridView ID="listaProductos" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-dark small">
                            <Columns>
                                <asp:BoundField DataField="idpersonal" HeaderText="ID" />
                                <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                                <asp:BoundField DataField="paterno" HeaderText="Paterno" />
                                <asp:BoundField DataField="materno" HeaderText="Materno" />
                                <asp:BoundField DataField="correo" HeaderText="Correo" />
                                <asp:BoundField DataField="telefono" HeaderText="Telefono" />
                                <asp:BoundField DataField="fechanac" HeaderText="Fecha Nacimiento" />
                                <asp:BoundField DataField="idjefe" HeaderText="ID jefe" />
                                <asp:BoundField DataField="idSucursal" HeaderText="ID sucursal" />
                                <asp:BoundField DataField="Tipo_Empleado" HeaderText="Tipo empleado" />
                                <asp:BoundField DataField="Salario" HeaderText="Salario" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" Text="Seleccionar" AutoPostBack="true" ID="lnkSelect" CommandArgument='<%# Eval("idpersonal") %>' OnClick="lnkSelect_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div>
                    </div>
                </div>
            </section>


            <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
                <div class="container text-center">
                    <small>Copyright 2020 &copy; Jorobas Beers</small>
                </div>
            </footer>

            <script src="../js/jquery-3.5.1.min.js"></script>
            <script src="../js/popper.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
        </div>
    </form>
</body>
</html>
