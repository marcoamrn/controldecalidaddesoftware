﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

namespace SJActualizado
{
    public partial class WebForm5 : System.Web.UI.Page
    {

        //creamos la conexion con el servidor
        readonly ConexionSQL conexionSQL = new ConexionSQL();
        readonly string querySQLSelectTablaProductos = "SELECT * from producto;";
        bool hayIdValido = false;


        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Application["personalActivo"] == false || (string)Application["nombreRolPersonalActivo"] != "Administrador")
            {
                //se redirecciona a su pagina de personal
                RedirigirAWeb();
            }
            conexionSQL.IntentarConexion();

            //rellenamos nuestra tabla 
            RellenarTablaQuery(listaProductos, querySQLSelectTablaProductos, conexionSQL);

            //dejamos nuestra alerta vacia
            cuadroConf.InnerText = "";

            //deshabilitamos nuestros botones
            btnActualizar.Enabled = false;
            btnEliminar.Enabled = false;
            //cerramos nuestra conexion
            conexionSQL.IntentarCerrarConexion();


        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();

            //leemos los textboxes y los guardamos en variables
            string nombreProductoA = nombreProducto.Text;
            string descripcionProductoA = descripcionProducto.Text;
            double precioproductoA = double.Parse(precioProducto.Text);
            int categoriaProductoA = catProducto.SelectedIndex + 1;


            string querySQLNomDescPrecioIdcat = "INSERT INTO producto(nombre, descripcion, precio, idcategoria) VALUES ('" + nombreProductoA + "','" + descripcionProductoA + "','" + precioproductoA + "','" + categoriaProductoA + "')";
            //indicamos a mySQL el query que queremos ejecutar con la conexion previamente abierta
            MySqlCommand comandoInsertar = new MySqlCommand(querySQLNomDescPrecioIdcat, conexionSQL.conex);
            //se ejecuta el query y se registra el numero de columnas modificadas
            int numFilasMod = comandoInsertar.ExecuteNonQuery();
            //verificamos el numero de columnas modificadas para mostrar mensajes
            if (numFilasMod < 1)
            {
                cuadroConf.InnerText = "Ha ocurrido un error";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }
            else
            {
                cuadroConf.InnerText = "Listo, elemento agregado";
                cuadroConf.Attributes.Add("class", "alert alert-success");
            }
            //actualizamos nuestra tabla
            RellenarTablaQuery(listaProductos, querySQLSelectTablaProductos, conexionSQL);

            //vaciamos los elementos de las text box
            VaciarElementos();

            //Final del script del boton
            conexionSQL.IntentarCerrarConexion();
        }


        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();

            //leemos los textboxes y los guardamos en variables
            string nombreProductoA = nombreProducto.Text;
            string descripcionProductoA = descripcionProducto.Text;
            double precioProductoA = double.Parse(precioProducto.Text);
            int catProductoA = catProducto.SelectedIndex + 1;

            string queryDelete = "DELETE FROM producto WHERE (nombre='" + nombreProductoA + "') AND (descripcion='" + descripcionProductoA + "') AND (idcategoria=" + catProductoA + ") AND (precio = " + precioProductoA + ")";
            MySqlCommand comandoDelete = new MySqlCommand(queryDelete, conexionSQL.conex);
            //se ejecuta el query y se registra el numero de columnas modificadas
            int numColMod = comandoDelete.ExecuteNonQuery();

            //verificamos el numero de columnas modificadas para mostrar mensajes
            if (numColMod < 1)
            {
                cuadroConf.InnerText = "Ha ocurrido un error: posiblemente el elemento que escribiste no existe, verifica los campos";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }
            else
            {
                cuadroConf.InnerText = "Listo, " + numColMod + " elemento/s borrado/s";
                cuadroConf.Attributes.Add("class", "alert alert-success");
            }

            //actualizamos nuestra tabla
            RellenarTablaQuery(listaProductos, querySQLSelectTablaProductos, conexionSQL);

            //vaciamos los elementos de las text box
            VaciarElementos();

            conexionSQL.IntentarCerrarConexion();
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();


            //leemos los textboxes y los guardamos en variables
            string nombreProductoA = nombreProducto.Text;
            string descripcionProductoA = descripcionProducto.Text;
            double precioProductoA = double.Parse(precioProducto.Text);
            int catProductoA = catProducto.SelectedIndex + 1;
            int idProductoA = int.Parse(idActualizar.Text);

            string queryActualizar = "UPDATE producto SET nombre='" + nombreProductoA + "', descripcion='" + descripcionProductoA + "', precio=" + precioProductoA + ", idcategoria=" + catProductoA + " WHERE idproducto = " + idProductoA + "";
            MySqlCommand comandoActualizar = new MySqlCommand(queryActualizar, conexionSQL.conex);
            //se ejecuta el query y se registra el numero de columnas modificadas
            int numColMod = comandoActualizar.ExecuteNonQuery();

            //verificamos el numero de columnas modificadas para mostrar mensajes
            if (numColMod < 1)
            {
                cuadroConf.InnerText = "Ha ocurrido un error: posiblemente el elemento que escribiste no existe, verifica los campos";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }
            else
            {
                cuadroConf.InnerText = "Listo, " + numColMod + " Elemento/s Actualizados/s";
                cuadroConf.Attributes.Add("class", "alert alert-success");
            }

            //actualizamos nuestra tabla
            RellenarTablaQuery(listaProductos, querySQLSelectTablaProductos, conexionSQL);

            //vaciamos los elementos de las text box
            VaciarElementos();

            conexionSQL.IntentarCerrarConexion();
        }



        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            //asignamos el id devuelto por la etiqueta select
            int productID = Convert.ToInt32((sender as LinkButton).CommandArgument);

            //mostramos un mensaje sobre el id que seleccionaron
            cuadroConf.InnerText = "Seleccionaste el producto con ID: " + productID.ToString();
            cuadroConf.Attributes.Add("class", "alert alert-info");

            //asignamos y ejecutamos el query
            string querySQLSELECTPID = "SELECT nombre, descripcion, idcategoria, precio FROM producto WHERE idproducto=" + productID;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };
            //creamos nuestra tabla de lectura y la llenamos
            DataTable table = new DataTable();
            adapter.Fill(table);

            //asignamos los valores de la consulta en sus respectivos campos
            nombreProducto.Text = table.Rows[0][0].ToString();
            descripcionProducto.Text = table.Rows[0][1].ToString();
            catProducto.SelectedIndex = (int.Parse(table.Rows[0][2].ToString())) - 1;
            precioProducto.Text = table.Rows[0][3].ToString();
            idActualizar.Text = productID.ToString();

            //encendemos los botones de actualizar y eliminar
            btnActualizar.Enabled = true;
            btnEliminar.Enabled = true;

        }

        protected void idActualizar_TextChanged(object sender, EventArgs e)
        {

            //deshabilitamos el cuadro de eventos
            cuadroConf.Attributes.Add("class", "");
            //asignamos el valor de id que escogimos
            int productID = 0;
            if (idActualizar.Text != "")
            {

                productID = int.Parse(idActualizar.Text);
            }
            //buscamos la fila con el id seleccionado
            string querySQLSELECTPID = "SELECT nombre, descripcion, idcategoria, precio FROM producto WHERE idproducto=" + productID;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            //ejecutamos el query
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };
            //creamos nuestra tabla de lectura y la llenamos con la informacion sql
            DataTable table = new DataTable();
            adapter.Fill(table);
            //En el caso de que nuestra tabla tenga informacion, llenamos nuestros campos con sus datos
            if (table.Rows.Count > 0)
            {
                nombreProducto.Text = table.Rows[0][0].ToString();
                descripcionProducto.Text = table.Rows[0][1].ToString();
                catProducto.SelectedIndex = (int.Parse(table.Rows[0][2].ToString())) - 1;
                precioProducto.Text = table.Rows[0][3].ToString();
                hayIdValido = true;
            }
            //si no hay informacion, vaciamos nuestros textbox
            else
            {
                VaciarElementos();
                hayIdValido = false;
            }
            //aqui activamos o desactivamos los botones act y eliminar
            if (hayIdValido)
            {
                btnActualizar.Enabled = true;
                btnEliminar.Enabled = true;
            }
            else
            {
                btnActualizar.Enabled = false;
                btnEliminar.Enabled = false;
            }

        }
        void RellenarTablaQuery(GridView tablaARellenar, string querySQL, ConexionSQL conexion)
        {
            //ejecutamos nuestro query 
            MySqlCommand comando = new MySqlCommand(querySQL, conexion.conex);
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };

            //creamos nuestra tabla de lectura
            DataTable table = new DataTable();
            adapter.Fill(table);

            //rellenamos nuestra tabla
            tablaARellenar.DataSource = table;
            tablaARellenar.DataBind();

        }
        void VaciarElementos()
        {

            //vaciamos nuestros texboxes 
            nombreProducto.Text = "";
            descripcionProducto.Text = "";
            catProducto.SelectedIndex = 0;
            precioProducto.Text = "";

        }
        public void IniciarSesionApp(string idPersonal, string rolPersonal)
        {
            Application["personalActivo"] = true;
            Application["nombreRolPersonalActivo"] = rolPersonal;
            Application["idPersonalActivo"] = idPersonal;
            RedirigirAWeb();
        }
        public void RedirigirAWeb()
        {
            if ((bool)Application["personalActivo"] == false)
            {
                Response.Redirect("login.aspx");
            }
            if ((string)Application["nombreRolPersonalActivo"] == "Administrador")
            {
                //redirigir a la pagina de administrador
                Response.Redirect("Pagina_Administrador.aspx");

            }
            else if ((string)Application["nombreRolPersonalActivo"] == "Mesero")
            {
                //redirigir a la pagina de meseros
                Response.Redirect("Pagina_Meseros.aspx");
            }

        }

        protected void botonCerrrSesion_Click(object sender, EventArgs e)
        {
            CerrarSesion();
        }
        public void CerrarSesion()
        {
            Application["personalActivo"] = false;
            Application["nombreRolPersonalActivo"] = "";
            Application["idPersonalActivo"] = 0;
            Response.Redirect("Home.aspx");
        }
    }
}