﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Data;

namespace SJActualizado
{
    public class Global : HttpApplication
    {
        public static DataTable TablaPreOrden = WebForm2.CrearColumnas();
        public void Application_Start(object sender, EventArgs e)
        {
            
            // Código que se ejecuta al iniciar la aplicación
            /*AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);*/
            //variable que define si esta o no un personal con sesion abierta
            Application["personalActivo"] = false;
            //variables del personal que tiene sesion abierta
            Application["nombreRolPersonalActivo"] = null;
            Application["idPersonalActivo"] = null;
            Application["idSucursal"] = null;
            Application["Aplicaciones"] = 0;
            Application["SesionesUsuario"] = 0;

            Application["Aplicaciones"] = (int)Application["Aplicaciones"] +1;
        }
        void Session_Start(object sender, EventArgs e)
        {
            Application["SesionesUsuario"] = (int)Application["SesionesUsuario"] + 1;
        }
        void Session_End(object sender, EventArgs e)
        {
            Application["SesionesUsuario"] = (int)Application["SesionesUsuario"] - 1;
        }
    }
}