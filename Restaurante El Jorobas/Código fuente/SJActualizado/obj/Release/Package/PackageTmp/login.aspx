﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="SJActualizado.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Jorobas Beers</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .form-signin {
            text-align: center;
        }
    </style>
</head>
<body class="bg-light text-center">
    <section class="container text-center">
        <div class="abs-center">
        <form class="form-signin" runat="server">
            <img class="mb-4" src="img/joro.jpg"  width="90%" height="90%">
            <h1 class="fs-2 mb-3 font-weight-bold">Iniciar Sesion</h1>
            <div id="alerta" runat="server" class=""></div>
            <select  runat="server" class="form-control" name="categoria" id="rolesDL">
                <option value="Administrador">Administrador</option>
                <option value="Mesero">Mesero</option>
            </select>
            <!--Cuadro Input Id empleado -->
            <label for="inputID" class="sr-only">ID de empleado</label>
            <asp:TextBox ID="inputID" runat="server" CssClass="form-control" placeholder="ID de empleado" required="" autofocus=""></asp:TextBox>
            <!--Cuadro Input contraseña -->
            <label for="inputPassword" class="sr-only">Contraseña</label>
            <asp:TextBox ID="inputPassword" runat="server" type="password" CssClass="form-control" placeholder="Contraseña" required=""></asp:TextBox>.
                
            <asp:Button ID="botonInicioSesion" runat="server" Text="Iniciar Sesión" CssClass="btn btn-dark btn-primary btn-block" OnClick="botonInicioSesion_Click" />
            
            <p class="mt-5 mb-3 text-muted">© 2019-2020</p>
          </form>
        </div>
    </section>
    <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
        <div class="container text-center">
          <small>Copyright 2020 &copy; Jorobas</small>
        </div>
    </footer>

    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
