﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin_agregarProductos.aspx.cs" Inherits="SJActualizado.agregarProductos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Agregar Productos</title>
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">

</head>
<body class="py-4" data-new-gr-c-s-check-loaded="14.994.0" data-gr-ext-installed="" cz-shortcut-listen="true">
    <form id="form1" runat="server">
        <div>
            <nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <div class="container">
            <a class="navbar-brand" href="index.html"><img src="img/100.png" class="logo-brand" alt="logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a class="nav-link" href="Home.aspx">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Info.aspx">Información</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Administrar menú
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="admin_agregarProductos.aspx">Modificar</a>
                        <a class="dropdown-item" href="mesero_verMenu.aspx">Mostrar</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Administrar personal
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Administrar sucursal
                    </a>
                </li>
                
                <li class="nav-item">
                    <asp:Button runat="server" ID="botonCerrrSesion" class="btn btn-dark" OnClick="botonCerrrSesion_Click" Text="Cerrar sesion" formnovalidate></asp:Button>
                </li>
            </ul>
            </div>
        </div>
    </nav>

        <section class="margTop">
            <div class="container py-5">
                <h1>Agregar productos</h1>
                <p class="lead">En esta seccion agregaremos los productos que se mostraran en el menú. <br></p>                
            </div>
            <div class="container ">
                <img src="img/comidaJorobas.png" class="img-thumbnail mx-auto d-block" width="55%"  alt="Menu">
            </div>
            <div class="container py-5 marL">
 
                <div class="row mb-4">
                    <div class="col-sm-3 col-lg-3 col-xl-3 themed-grid-col">Nombre</div>
                    <div class="col-sm-3 col-lg-3 col-xl-3 themed-grid-col">Descripcion</div>
                    <div class="col-sm-3 col-lg-3 col-xl-3 themed-grid-col">Categoria</div>
                    <div class="col-sm-3 col-lg-3 col-xl-3 themed-grid-col">Precio</div>
                </div>
                <div class="row mb-4">
                    <div class=" col-sm-3 col-lg-3 col-xl-3 "><asp:Textbox runat="server" ID="nombreProducto" type="text" class="form-control" name="nombre" placeholder="Nombre" required="" ></asp:Textbox></div> 
                    <div class=" col-sm-3 col-lg-3 col-xl-3"><asp:Textbox runat="server" ID="descripcionProducto" type="text" class="form-control" name="descripcion" placeholder="Descripcion" required=""></asp:Textbox></div>
                    <div class=" col-sm-3 col-lg-3 col-xl-3"><name="categoria" id="" placeholder="Categoria">
                        <select  runat="server" class="form-control" name="categoria" id="catProducto" placeholder="-Selecciona un elemento-" onselect="asignarValSelect">
                            
                            <option value="Comida">Comida</option>
                            <option value="Bebida">Bebida</option>
                        </select>
                        
                    </div>
                    <div class=" col-sm-3 col-lg-3 col-xl-3"><asp:Textbox runat="server" ID="precioProducto" type="number" class="form-control" name="precio" placeholder="$" required=""></asp:Textbox></div>
                </div>

                <div class="container py-5">
                    <div class="col text-center ">
                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar como nuevo producto" CssClass="btn btn-primary" OnClick="btnGuardar_Click"/>
                    </div>
                    <br />
                    <center>

                        <asp:TextBox ID="idActualizar" runat="server" type="number" class="form-group col-md-2" name="id" AutoPostBack="true" placeholder="ID" OnTextChanged="idActualizar_TextChanged" ></asp:TextBox>
                    </center>
                    <div class="col text-center ">
                        
                        <asp:Button ID="btnEliminar" runat="server" Text="Eliminar por ID" CssClass="btn btn-primary" OnClick="btnEliminar_Click"/>
                        <asp:Button ID="btnActualizar" runat="server" Text="Actualizar por ID" CssClass="btn btn-primary" OnClick="btnActualizar_Click"/>
                        
                    </div>

                </div> 
                <div>
                    <div id="cuadroConf" runat="server" class=""></div>
                    <asp:GridView ID="listaProductos" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-dark" >
                    <Columns>
                        <asp:BoundField DataField="idproducto" HeaderText="ID Producto" />
                        <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                        <asp:BoundField DataField="descripcion" HeaderText="Descripcion" />
                        <asp:BoundField DataField="precio" HeaderText="Precio" />
                        <asp:BoundField DataField="idCategoria" HeaderText="ID Categoria" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton Text="Seleccionar" ID="lnkSelect" CommandArgument='<%# Eval("idproducto") %>' runat="server" OnClick="lnkSelect_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </div>
                <div>
                    
                </div>
            </div>
        </section>


        <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
            <div class="container text-center">
                <small>Copyright 2020 &copy; Jorobas Beers</small>
            </div>
        </footer>

            <script src="../js/jquery-3.5.1.min.js"></script>
            <script src="../js/popper.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
        </div>
    </form>
</body>
</html>
