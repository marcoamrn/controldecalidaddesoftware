﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pagina_Meseros.aspx.cs" Inherits="SJActualizado.pagina_meseros" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Cuenta</title>
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body class="py-4" data-new-gr-c-s-check-loaded="14.994.0" data-gr-ext-installed="" cz-shortcut-listen="true">
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="index.html"><img src="img/100.png" class="logo-brand" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
        
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="Home.aspx">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Info.aspx">Información</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="mesero_verMenu.aspx" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Menú
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="Meseros_Cuenta.aspx" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Cuenta
                            </a>
                        </li>
                        <li class="nav-item">
                            <asp:button runat="server" class="btn btn-primary btn-dark" Text="Cerrar sesion" OnClick="botonCerrrSesion_Click"></asp:button>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>
        <div class="card text-dark bg-light margTop py-5">
        <div class="card-body text-center ">
            <h1 runat="server" id="mensajeBienvenida" class="jumbotron-heading">Bienvenido ******** </h1>
            <p class="card-text font-italic">                
                "Nunca pienso en las consecuencias de fallar un gran tiro… 
                cuando se piensa en las consecuencias se está pensando en un resultado negativo". <br> <h6>- Michael Jordan</h6> 
            </p>
        </div>        
        <img class="mx-auto d-block" src="img/restaurant.jpg" alt="Comida">

    </div>



    <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
        <div class="container text-center">
            <small>Copyright 2020 &copy; Jorobas Beers</small>
        </div>
    </footer>
    </form>
</body>
</html>
