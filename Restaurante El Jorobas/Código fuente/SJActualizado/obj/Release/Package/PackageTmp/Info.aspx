﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Info.aspx.cs" Inherits="SJActualizado.WebForm4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Contacto</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Sulphur+Point:700&display=swap" rel="stylesheet">
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/grid.css">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="home.html"><img src="img/100.png" class="logo-brand" alt="logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a class="nav-link" href="Home.aspx">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Info.aspx">Información</a>
                </li>
                <li class="nav-item">
                    <asp:Button runat="server" ID="iniciarSesion" OnClick="iniciarSesion_Click" class="btn btn-primary btn-dark" Text="Iniciar sesion"></asp:Button>
                </li>
            </ul>
            </div>
        </div>
    </nav>

    <section class="text-center py-5">
        <div class="container">
          <h1 class="jumbotron-heading">Contacto</h1>
        </div>
    </section>

    <section>
        <div class="container py-1 marL ">
            <div class="row justify-content-between">
                <div class="col-sm-12 col-lg-6 align-self-center">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d17178.006584562314!2d-98.28710866634327!3d19.058729276009093!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfc64cbe125ebb%3A0x9036070748e6ef4e!2sJorobas%20Beers!5e0!3m2!1ses!2smx!4v1633842934726!5m2!1ses!2smx" 
                    width="100%" height="600" allowfullscreen="" loading="lazy"></iframe>
                </div>

                <div class="col-sm-12 col-lg-6 margDes">
                    <div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <h4>Dudas o aclaraciones</h4>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <div class="row">
                                <div class="col-sm-12 col-lg-6">
                                    <p class="lead"><b>2222254202</b></p>    
                                </div>
                                <div class="btn-group-toggle col-sm-12 col-lg-6">
                                    <label onclick="location.href='https://www.google.com'" class="btn btn-secondary active bg-dark">
                                        <input type="checkbox" checked autocomplete="off"> Llamar
                                    </label>   
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <p class="lead text-justify margDes ">
                        En Jorobas Beers, estamos decididos a brindarle productos durareros y de alta 
                        calidad a todos nuestros clientes. Buscamos brindarle el mejor servicio del mercado.
                        Adoptando todas las medidas de seguridad, nuestro equipo de servicio al cliente, 
                        esta listo para atenderte por el canal de tu preferencia: Teléfono o Chat. <br>
                        Nuestros asesores estan listos para atenderte con cualquier consulta sobre tu 
                        pedido o información en general. <br>
                        Los horarios son los siguientes: <br>
                        <b>Lunes a Viernes - 12 pm a 10 pm</b> <br>
                        <b>Sábado y Domingo - 10 am a 11 pm</b> 
                        <br>
                        Acerca de nosotros <br>
                        Jorobas Beers. <br>
                        <b>Direccion</b> <br>
                        Calle 14 Ote 1821, Floral, Zona de la Universidad de las Américas, 72810 San Andrés Cholula, Pue. <br>
                        C.P. 72810 <br>
                    </p>
                </div>
            </div>
        </div>
    </section>
    

    <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
        <div class="container text-center">
            <small>Copyright 2020 &copy; Jorobas Beers</small>
        </div>
    </footer>

        <script src="../js/jquery-3.5.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        </div>
    </form>
</body>
</html>
