﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="SJActualizado.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Jorobas Beers</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Sulphur+Point:700&display=swap" rel="stylesheet">
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/grid.css">
</head>
<body class="py-4" data-new-gr-c-s-check-loaded="14.994.0" data-gr-ext-installed="" cz-shortcut-listen="true">
    <form id="form1" runat="server">
        <div>
            <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="home.html"><img src="img/100.png" class="logo-brand" alt="logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a class="nav-link" href="Home.aspx">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Info.aspx">Información</a>
                </li>
                <li class="nav-item">
                    <asp:Button runat="server" ID="iniciarSesion" OnClick="iniciarSesion_Click" class="btn btn-primary btn-dark" Text="Iniciar sesion"></asp:Button>
                </li>
            </ul>
            </div>
        </div>
    </nav>

    <section class="bg-burger">
        <div class="container py-6">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h1 class="text-dark display-3 mb-5 py-5">Jorobas Beers</h1>
                    <p class="lead text-justify text-dark mb-5">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet iste quis necessitatibus. 
                        Vero porro perferendis consectetur natus necessitatibus architecto repudiandae impedit 
                        reprehenderit, dolorem mollitia itaque nulla molestiae fuga ut commodi.
                    </p>
                </div>
                <div class="col-lg-6 col-md-12">
                    <img src="img/burger.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
        
    <section class="margTop py-6">
        <div class="container">
            <h2 class="display-4 mb-4">Nuestro Equipo</h2>
            <div class="row">
                <div class="col-lg-3 col-md-12 text-center my-3">
                    <div class="category position-relative carreaux_presentation_light">
                        <div class=" category-description position-absolute text-light text-center w-75">
                            <h4 class="deroul_titre rounded">Emma</h4>
                            <p class="deroul_soustitre rounded">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                        <img src="img/Ema.png" class="img-fluid rounded" alt="Emanuel">
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 my-3">
                    <div class="category position-relative carreaux_presentation_light">
                        <div class="category-description position-absolute text-center text-light w-75">
                            <h4 class="deroul_titre rounded">Marquinho</h4>
                            <p class="deroul_soustitre rounded">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                        <img src="img/Marquinho.png" class="img-fluid rounded" alt="Marco">
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 my-3">
                    <div class="category position-relative carreaux_presentation_light">
                        <div class="category-description position-absolute text-center text-light w-75 hov-text">
                            <h4 class="deroul_titre rounded">Migue</h4>
                            <p class="deroul_soustitre rounded">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                        <img src="img/Migue.png" class="img-fluid rounded" alt="Miguel">
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 text-center my-3">
                    <div class="category position-relative carreaux_presentation_light">
                        <img src="img/Oreo.png" class="img-fluid rounded" alt="Orestes">
                        <div class="category-description position-absolute text-center text-light w-75">
                            <h4 class="deroul_titre rounded">Oreo</h4>
                            <p class="deroul_soustitre rounded">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="carreaux_presentation_light" style="background-image:url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/173024/jonathan_larradet_FLATDESIGN_250x250.png);">
        <div class="shadow_swhow_mini">
            <div class="deroul_titre">PURE CSS :HOVER</div>
            <div class="deroul_soustitre">www.wifeo.com/code</div>
        </div>
      </div>

    <section class="jumbotron text-center bg-burger">
        <div>
          <h1 class="lead fs-1">Bienvenidos al mejor restaurant de Puebla <br> Los esperamos <br> Proximamente </h1>
        </div>
    </section>

    <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
        <div class="container text-center">
            <small>Copyright 2020 &copy; Jorobas Beers</small>
        </div>
    </footer>

        <script src="../js/jquery-3.5.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        </div>
    </form>
</body>
</html>
