﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Meseros_Cuenta.aspx.cs" Inherits="SJActualizado.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Cuenta</title>
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <nav class="navbar navbar-expand-lg navbar-light fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="index.html"><img src="img/100.png" class="logo-brand" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
        
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="Home.aspx">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Info.aspx">Información</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="mesero_verMenu.aspx">
                                Menú
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="Meseros_Cuenta.aspx">
                                Cuenta
                            </a>
                        </li>
                        <li class="nav-item">
                            <asp:button runat="server" class="btn btn-primary btn-dark" Text="Cerrar sesion" OnClick="botonCerrrSesion_Click" formnovalidate></asp:button>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>


            <div class="container-fluid margTop">
                <div class="row py-5">
                    <div class="col-11 mx-auto pt-3">
                        <!-- Titulo -->

                        <h3 class="text-uppercase mb-4">Cuenta</h3>
                        <div class="row py-2">
                            <div class="col-md-7">
                                <!-- Ifo general -->
                                <div class="row my-2">
                                    <div class="col-4 text-center mb-2">
                                        <h6 class="text-uppercase info-title">Pago</h6>
                                        <span class="budget-icon"><i class="fas fa-money-bill-alt"></i></span>
                                        <h4 class="text-uppercase mt-2 budget" id="pago">
                                            <span>$ </span>
                                            <asp:TextBox ID="presupuestoC" runat="server" type="number" AutoPostBack="true" CssClass=" expense-input" Width="80px" OnTextChanged="presupuestoC_TextChanged"></asp:TextBox>
                                        </h4>
                                    </div>
                                    <div class="col-4 text-center">
                                        <h6 class="text-uppercase info-title">Acumulado</h6>
                                        <span class="expense-icon"><i class="far fa-credit-card"></i></span>
                                        <h4 class="text-uppercase mt-2 expense" id="gasto"><span>$ </span><span runat="server"
                                                id="acumulado">0</span></h4>
                                    </div>
                                    <div class="col-4 text-center">
                                        <h6 class="text-uppercase info-title">Cambio</h6>
                                        <span class="balance-icon"><i class="fas fa-dollar-sign"></i></span>
                                        <h4 class="text-uppercase mt-2 balance" id="total"> 
                                            <span>$ </span>
                                            <span runat="server" id="textoCambio">0</span>

                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5 my-3">
                                <!-- expense form -->
                                <div id="expense-form" class=" expense-form">
                                    <h5 class="text-capitalize">Id del producto</h5>
                                    <div class="form-group">
                                        <asp:TextBox ID="idProducto" runat="server" required="" type="number" class="form-control expense-input" name="id" AutoPostBack="true" placeholder="ID" OnTextChanged="idProducto_TextChanged"></asp:TextBox>
                                        
                                    </div>
                                    <h5 class="text-capitalize">Número de unidades</h5>
                                    <div class="form-group">
                                        <asp:TextBox ID="numUnidades" runat="server" required="" type="number" CssClass="form-control expense-input" name="id" AutoPostBack="true" placeholder="Unidades" OnTextChanged="numUnidades_TextChanged"></asp:TextBox>
                                        
                                    </div>
                                    <!-- Boton para agregar elemento a la cuenta-->
                                    <div class="container py-5">
                                        <div class="col text-center">
                                               <asp:button runat="server" ID="btnAgregar" class="btn btn-primary" OnClick="btnAgregar_Click" Text="Agregar a cuenta"></asp:button>
                                        </div>
                                    </div> 


                                </div>
                            </div>
                            <div class="col-md-7 my-3">
                                <!-- Lista de consumo -->
                                <div class="expense-list" id="expense-list">

                                    <div class="expense-list__info d-flex justify-content-between text-capitalize">
                                        <h5 class="list-item">Producto</h5>
                                        <h5 class="list-item">Gasto</h5>
                                        <h5 class="list-item"></h5>
                                    </div>
                                    <div class="expense-list__info d-flex justify-content-between text-capitalize">
                                        <h5 runat="server" class="list-item" id="productoTexto"></h5>
                                        <h5 runat="server" class="list-item" id="gastoTexto" ></h5>
                                        <h5 runat="server" class="list-item"></h5>
                                    </div>
                                    <asp:GridView ID="listaCuenta" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-dark" >
                                        <Columns>
                                            <asp:BoundField DataField="idProducto" HeaderText="ID Producto" />
                                            <asp:BoundField DataField="nombreProducto" HeaderText="Nombre" />
                                            <asp:BoundField DataField="cantidadProducto" HeaderText="cantidad" />
                                            <asp:BoundField DataField="precioTotalProductos" HeaderText="total" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Button ID="efectuarVenta" runat="server" Text="Generar venta" CssClass="btn btn-success" Enabled="false" display="none"  OnClick="efectuarVenta_Click" formnovalidate   />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
                <div class="container text-center">
                    <small>Copyright 2020 &copy; Jorobas Beers</small>
                </div>
            </footer>

                <script src="../js/jquery-3.5.1.min.js"></script>
                <script src="../js/popper.min.js"></script>
                <script src="../js/bootstrap.min.js"></script>
        </div>
    </form>
</body>
</html>
