﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mesero_verMenu.aspx.cs" Inherits="SJActualizado.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Menú</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            
            <nav runat="server" class="navbar navbar-expand-lg navbar-light fixed-top" id="navMesero">
                <div class="container">
                    <a class="navbar-brand" href="index.html"><img src="img/100.png" class="logo-brand" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
        
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="Home.aspx">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Info.aspx">Información</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="mesero_verMenu.aspx">
                                Menú
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="Meseros_Cuenta.aspx">
                                Cuenta
                            </a>
                        </li>
                        <li class="nav-item">
                            <asp:button runat="server" class="btn btn-primary btn-dark" Text="Cerrar sesion" OnClick="botonCerrrSesion_Click"></asp:button>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>

            <nav runat="server" class="navbar navbar-expand-lg navbar-light fixed-top " id="navAdmin" >
                <div class="container">
                    <a class="navbar-brand" href="index.html"><img src="img/100.png" class="logo-brand" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
        
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                        <a class="nav-link" href="Home.aspx">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Info.aspx">Información</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Administrar menu
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="admin_agregarProductos.aspx">Modificar</a>
                                <a class="dropdown-item" href="#">Mostrar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Administrar personal
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Administrar sucursal
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contacto.html">Ventas</a>
                        </li>
                        <li class="nav-item">
                            <asp:Button runat="server" ID="botonCerrrSesion" class="btn btn-dark" OnClick="botonCerrrSesion_Click" Text="Cerrar sesion"></asp:Button>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>

            <section class="margTop">
                <div class="container py-5">
                    <h1 class="text-center">Menú</h1>
                </div>
                <div class="container ">
                    <img src="img/menuCompleto.png" class="img-thumbnail mx-auto d-block fluid" width="55%"  alt="Menu">
                    <p class="py-3 mb-5 text-center">
                        Productos ofrecidos en nuestro restaurante: <br>
                    </p>


                    <asp:GridView ID="listaProductosMeseros" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-dark" >
                        <Columns>
                            <asp:BoundField DataField="idproducto" HeaderText="ID Producto" />
                            <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                            <asp:BoundField DataField="descripcion" HeaderText="Descripcion" />
                            <asp:BoundField DataField="precio" HeaderText="Precio" />
                            
                        </Columns>

                    </asp:GridView>
                </div>
            </section>
        


            <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
                <div class="container text-center">
                    <small>Copyright 2020 &copy; Jorobas Beers</small>
                </div>
            </footer>

                <script src="../js/jquery-3.5.1.min.js"></script>
                <script src="../js/popper.min.js"></script>
                <script src="../js/bootstrap.min.js"></script>

        </div>
    </form>
</body>
</html>
