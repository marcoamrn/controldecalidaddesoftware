﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pagina_Administrador.aspx.cs" Inherits="SJActualizado.Pagina_Administrador" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Admin</title>
    <link rel="icon" href="img/sinFondo.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grid.css">
</head>
<body class="py-4" data-new-gr-c-s-check-loaded="14.994.0" data-gr-ext-installed="" cz-shortcut-listen="true">
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
        <div class="container">
            <a class="navbar-brand" href="index.html"><img src="img/100.png" class="logo-brand" alt="logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a class="nav-link" href="index.aspx">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Info.aspx">Información</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Administrar menú
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="admin_agregarProductos.aspx">Modificar</a>
                        <a class="dropdown-item" href="mesero_verMenu.aspx">Mostrar</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="administrarPersonal.aspx">
                        Administrar personal
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="administrarSucursales.aspx" >
                        Administrar sucursal
                    </a>
                </li>
                
                <li class="nav-item">
                    <asp:Button runat="server" ID="botonCerrrSesion" class="btn btn-dark" OnClick="botonCerrrSesion_Click" Text="Cerrar sesion"></asp:Button>
                </li>
            </ul>
            </div>
        </div>
    </nav>

    
    <div class="card text-dark bg-light margTop py-5">
        <div class="card-body text-center ">
            <h1 runat="server" id="mensajeBienvenida" class="jumbotron-heading">Bienvenido ******** </h1>
            <p class="card-text font-italic">                
                "Nunca pienso en las consecuencias de fallar un gran tiro… 
                cuando se piensa en las consecuencias se está pensando en un resultado negativo". <br> <h6>- Michael Jordan</h6> 
            </p>
        </div>        
        <img class="mx-auto d-block" src="img/restaurant.jpg" alt="Comida">

    </div>
        



    <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
        <div class="container text-center">
            <small>Copyright 2020 &copy; Jorobas Beers</small>
        </div>
    </footer>

        <script src="../js/jquery-3.5.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
    </form>
    
</body>
</html>