﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace SJActualizado
{
    public class ConexionSQL : System.Web.UI.Page
    {
        
        
        static readonly string servidor = "localhost";
        static readonly string bd = "jorobasdbeer";
        static readonly string usuario = "miguelish";
        static readonly string password = "p@$$w0rd";
        public string mensajeError;

        static readonly string cadConexion = "server="+servidor+";uid="+usuario+";pwd="+password+";database="+bd+";SSL Mode=None";
        public MySqlConnection conex = new MySqlConnection(cadConexion);

        public MySqlConnection IntentarConexion()
        {
            try
            {
                conex.Open();

            }
            catch (MySqlException excep)
            {
                //se usa para mostrar errores

            }
            return conex;
        }
        public MySqlConnection IntentarCerrarConexion()
        {
            try
            {
                conex.Close();

            }
            catch (MySqlException excep)
            {
                //se usa para mostrar errores

            }
            return conex;
        }

    }
}