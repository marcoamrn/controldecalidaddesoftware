﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

namespace SJActualizado
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        //creamos la conexion con el servidor
        readonly ConexionSQL conexionSQL = new ConexionSQL();
        readonly string querySQLSelectTablaSucursales = "SELECT * from sucursal;";
        bool hayIdValido = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Application["personalActivo"] == false || (string)Application["nombreRolPersonalActivo"] != "Administrador")
            {
                //se redirecciona a su pagina de personal
                RedirigirAWeb();
            }
            conexionSQL.IntentarConexion();

            //rellenamos nuestra tabla 
            RellenarTablaQuery(listaSucursales, querySQLSelectTablaSucursales, conexionSQL);

            //dejamos nuestra alerta vacia
            cuadroConf.InnerText = "";

            //deshabilitamos nuestros botones
            btnActualizar.Enabled = false;
            btnEliminar.Enabled = false;
            //cerramos nuestra conexion
            conexionSQL.IntentarCerrarConexion();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();

            //leemos los textboxes y los guardamos en variables
            string textboxNombreSuc_ = textboxNombreSuc.Text;
            string textboxDireccionSuc_ = textboxDireccionSuc.Text;
            string textboxEmail_ = textboxEmail.Text;
            string textboxTelefonoSuc_ = textboxTelefonoSuc.Text;
            string textboxCodigoP_ = textboxCodigoP.Text;


            string querySQLNomDescPrecioIdcat = "INSERT INTO sucursal(nombre, telsucursal, cp, calle, email) VALUES ('" + textboxNombreSuc_ + "','" + textboxTelefonoSuc_ + "','" + textboxCodigoP_ + "','" + textboxDireccionSuc_ + "','" + textboxEmail_ + "')";
            //indicamos a mySQL el query que queremos ejecutar con la conexion previamente abierta
            MySqlCommand comandoInsertar = new MySqlCommand(querySQLNomDescPrecioIdcat, conexionSQL.conex);
            //se ejecuta el query y se registra el numero de columnas modificadas
            int numFilasMod = comandoInsertar.ExecuteNonQuery();
            //verificamos el numero de columnas modificadas para mostrar mensajes
            if (numFilasMod < 1)
            {
                cuadroConf.InnerText = "Ha ocurrido un error";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }
            else
            {
                cuadroConf.InnerText = "Listo, elemento agregado";
                cuadroConf.Attributes.Add("class", "alert alert-success");
            }
            //actualizamos nuestra tabla
            RellenarTablaQuery(listaSucursales, querySQLSelectTablaSucursales, conexionSQL);

            //vaciamos los elementos de las text box
            VaciarElementos();

            //Final del script del boton
            conexionSQL.IntentarCerrarConexion();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();

            

            //leemos los textboxes y los guardamos en variables
            string textboxNombreSuc_ = textboxNombreSuc.Text;
            string textboxDireccionSuc_ = textboxDireccionSuc.Text;
            string textboxEmail_ = textboxEmail.Text;
            string textboxTelefonoSuc_ = textboxTelefonoSuc.Text;
            string textboxCodigoP_ = textboxCodigoP.Text;

            int idSucursal = 0;
            if (idActualizar.Text != "")
            { idSucursal = int.Parse(idActualizar.Text); }
            string queryDelete = "DELETE FROM sucursal WHERE idSucursal = "+ idSucursal;
            MySqlCommand comandoDelete = new MySqlCommand(queryDelete, conexionSQL.conex);
            //se ejecuta el query y se registra el numero de columnas modificadas



            int numColMod = comandoDelete.ExecuteNonQuery();

            //verificamos el numero de columnas modificadas para mostrar mensajes
            if (numColMod < 1)
            {
                cuadroConf.InnerText = "Ha ocurrido un error: posiblemente el elemento que escribiste no existe, verifica los campos";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }
            else
            {
                cuadroConf.InnerText = "Listo, " + numColMod + " elemento/s borrado/s";
                cuadroConf.Attributes.Add("class", "alert alert-success");
            }
            


            //actualizamos nuestra tabla
            RellenarTablaQuery(listaSucursales, querySQLSelectTablaSucursales, conexionSQL);

            //vaciamos los elementos de las text box
            VaciarElementos();

            conexionSQL.IntentarCerrarConexion();
        }


        protected void idActualizar_TextChanged(object sender, EventArgs e)
        {

            //deshabilitamos el cuadro de eventos
            cuadroConf.Attributes.Add("class", "");
            //asignamos el valor de id que escogimos
            int idSucursal = 0;
            if (idActualizar.Text != "")
            { idSucursal = int.Parse(idActualizar.Text); }
            //buscamos la fila con el id seleccionado
            string querySQLSELECTPID = "SELECT nombre, telsucursal, cp, calle, email FROM sucursal WHERE idSucursal=" + idSucursal;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            //ejecutamos el query
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };
            //creamos nuestra tabla de lectura y la llenamos con la informacion sql
            DataTable table = new DataTable();
            adapter.Fill(table);
            //En el caso de que nuestra tabla tenga informacion, llenamos nuestros campos con sus datos
            if (table.Rows.Count > 0)
            {
                textboxNombreSuc.Text = table.Rows[0][0].ToString();
                textboxTelefonoSuc.Text = table.Rows[0][1].ToString();
                textboxCodigoP.Text = table.Rows[0][2].ToString();
                textboxDireccionSuc.Text = table.Rows[0][3].ToString();
                textboxEmail.Text = table.Rows[0][4].ToString();
                hayIdValido = true;
            }
            //si no hay informacion, vaciamos nuestros textbox
            else
            {
                VaciarElementos();
                hayIdValido = false;
            }
            //aqui activamos o desactivamos los botones act y eliminar
            if (hayIdValido)
            {
                btnActualizar.Enabled = true;
                btnEliminar.Enabled = true;
            }
            else
            {
                btnActualizar.Enabled = false;
                btnEliminar.Enabled = false;
            }

        }
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            conexionSQL.IntentarConexion();


            //leemos los textboxes y los guardamos en variables
            string textboxNombreSuc_ = textboxNombreSuc.Text;
            string textboxDireccionSuc_ = textboxDireccionSuc.Text;
            string textboxEmail_ = textboxEmail.Text;
            string textboxTelefonoSuc_ = textboxTelefonoSuc.Text;
            string textboxCodigoP_ = textboxCodigoP.Text;
            int idSucursal = 0;
            if (idActualizar.Text != "")
            { idSucursal = int.Parse(idActualizar.Text); }
            string queryActualizar = "UPDATE sucursal SET nombre='" + textboxNombreSuc_ + "', calle='" + textboxDireccionSuc_ + "', email='" + textboxEmail_ + "', telsucursal='" + textboxTelefonoSuc_ +"', cp='" + textboxCodigoP_ + "' WHERE idSucursal = " + idSucursal;
            MySqlCommand comandoActualizar = new MySqlCommand(queryActualizar, conexionSQL.conex);
            //se ejecuta el query y se registra el numero de columnas modificadas
            int numColMod = comandoActualizar.ExecuteNonQuery();

            //verificamos el numero de columnas modificadas para mostrar mensajes
            if (numColMod < 1)
            {
                cuadroConf.InnerText = "Ha ocurrido un error: posiblemente el elemento que escribiste no existe, verifica los campos";
                cuadroConf.Attributes.Add("class", "alert alert-danger");
            }
            else
            {
                cuadroConf.InnerText = "Listo, " + numColMod + " Elemento/s Actualizados/s";
                cuadroConf.Attributes.Add("class", "alert alert-success");
            }

            //actualizamos nuestra tabla
            RellenarTablaQuery(listaSucursales, querySQLSelectTablaSucursales, conexionSQL);

            //vaciamos los elementos de las text box
            VaciarElementos();

            conexionSQL.IntentarCerrarConexion();
        }
        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            //asignamos el id devuelto por la etiqueta select
            int idSucursal = Convert.ToInt32((sender as LinkButton).CommandArgument);

            //mostramos un mensaje sobre el id que seleccionaron
            cuadroConf.InnerText = "Seleccionaste el producto con ID: " + idSucursal.ToString();
            cuadroConf.Attributes.Add("class", "alert alert-info");

            //asignamos y ejecutamos el query
            string querySQLSELECTPID = "SELECT nombre, telsucursal, cp, calle, email FROM sucursal WHERE idSucursal=" + idSucursal;
            MySqlCommand comando = new MySqlCommand(querySQLSELECTPID, conexionSQL.conex);
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };
            //creamos nuestra tabla de lectura y la llenamos
            DataTable table = new DataTable();
            adapter.Fill(table);

            //asignamos los valores de la consulta en sus respectivos campos
            textboxNombreSuc.Text = table.Rows[0][0].ToString();
            textboxTelefonoSuc.Text = table.Rows[0][1].ToString();
            textboxCodigoP.Text = table.Rows[0][2].ToString();
            textboxDireccionSuc.Text = table.Rows[0][3].ToString();
            textboxEmail.Text = table.Rows[0][4].ToString();
            idActualizar.Text = idSucursal.ToString();

            //encendemos los botones de actualizar y eliminar
            btnActualizar.Enabled = true;
            btnEliminar.Enabled = true;

        }

        void RellenarTablaQuery(GridView tablaARellenar, string querySQL, ConexionSQL conexion)
        {
            //ejecutamos nuestro query 
            MySqlCommand comando = new MySqlCommand(querySQL, conexion.conex);
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };

            //creamos nuestra tabla de lectura
            DataTable table = new DataTable();
            adapter.Fill(table);

            //rellenamos nuestra tabla
            tablaARellenar.DataSource = table;
            tablaARellenar.DataBind();

        }
        void VaciarElementos()
        {

            //vaciamos nuestros texboxes 
            textboxNombreSuc.Text = "";
            textboxDireccionSuc.Text = "";
            textboxEmail.Text = "";
            textboxTelefonoSuc.Text = "";
            textboxCodigoP.Text = "";

        }
        protected void botonCerrrSesion_Click(object sender, EventArgs e)
        {
            CerrarSesion();
        }
        public void CerrarSesion()
        {
            Application["personalActivo"] = false;
            Application["nombreRolPersonalActivo"] = "";
            Application["idPersonalActivo"] = 0;
            Response.Redirect("index.aspx");
        }
        public void RedirigirAWeb()
        {
            if ((bool)Application["personalActivo"] == false)
            {
                Response.Redirect("login.aspx");
            }
            if ((string)Application["nombreRolPersonalActivo"] == "Administrador")
            {
                //redirigir a la pagina de administrador
                Response.Redirect("Pagina_Administrador.aspx");

            }
            else if ((string)Application["nombreRolPersonalActivo"] == "Mesero")
            {
                //redirigir a la pagina de meseros
                Response.Redirect("Pagina_Meseros.aspx");
            }

        }
    }

}