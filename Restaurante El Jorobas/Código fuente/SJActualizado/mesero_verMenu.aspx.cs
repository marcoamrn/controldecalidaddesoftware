﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace SJActualizado
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        
        readonly ConexionSQL conexionSQL = new ConexionSQL();
        readonly string querySQLSelectTablaProductos = "SELECT idproducto, nombre, descripcion, precio from producto;";
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Application["personalActivo"] == false)
            {
                //se redirecciona a su pagina de personal o login
                RedirigirAWeb();
            }

            if((string)Application["nombreRolPersonalActivo"] == "Mesero")
            {
                navAdmin.Style.Add("display", "none");
            }
            else if((string)Application["nombreRolPersonalActivo"] == "Administrador")
            {
                navMesero.Style.Add("display", "none");
            }
            conexionSQL.IntentarConexion();
            //rellenamos nuestra tabla 
            RellenarTablaQuery(listaProductosMeseros, querySQLSelectTablaProductos, conexionSQL);

            //dejamos nuestra alerta vacia
            //cuadroConf.InnerText = "";

            //deshabilitamos nuestros botones
            //btnActualizar.Enabled = false;
            //btnEliminar.Enabled = false;
            //cerramos nuestra conexion
            conexionSQL.IntentarCerrarConexion();
        }


        void RellenarTablaQuery(GridView tablaARellenar, string querySQL, ConexionSQL conexion)
        {
            //ejecutamos nuestro query 
            MySqlCommand comando = new MySqlCommand(querySQL, conexion.conex);
            MySqlDataAdapter adapter = new MySqlDataAdapter
            {
                SelectCommand = comando
            };

            //creamos nuestra tabla de lectura
            DataTable table = new DataTable();
            adapter.Fill(table);

            //rellenamos nuestra tabla
            tablaARellenar.DataSource = table;
            tablaARellenar.DataBind();

        }
        void RedirigirAWeb()
        {
            if ((bool)Application["personalActivo"] == false)
            {
                Response.Redirect("login.aspx");
            }
            if ((string)Application["nombreRolPersonalActivo"] == "Administrador")
            {
                //redirigir a la pagina de administrador
                Response.Redirect("Pagina_Administrador.aspx");

            }
            else if ((string)Application["nombreRolPersonalActivo"] == "Mesero")
            {
                //redirigir a la pagina de meseros
                Response.Redirect("Pagina_Meseros.aspx");
            }
        }

        protected void botonCerrrSesion_Click(object sender, EventArgs e)
        {
            CerrarSesion();
        }
        public void CerrarSesion()
        {
            Application["personalActivo"] = false;
            Application["nombreRolPersonalActivo"] = "";
            Application["idPersonalActivo"] = 0;
            Response.Redirect("index.aspx");
        }
    }
}