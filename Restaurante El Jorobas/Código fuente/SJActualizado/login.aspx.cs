﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;


namespace SJActualizado
{
    public partial class login : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((bool)Application["personalActivo"] == true)
            {
                //se redirecciona a su pagina de personal
                RedirigirAWeb();
            }
        }

        protected void botonInicioSesion_Click(object sender, EventArgs e)
        {
            //creamos la conexion con el servidor
            ConexionSQL conexionSQL = new ConexionSQL();
            //verificamos que se pueda realizar la conexion correctamente
            
            conexionSQL.conex.Open();

            
            string rol = rolesDL.Value;
            string contrasena = inputPassword.Text;
            string idPersonal = inputID.Text;
            
            

            
            
            string querysql = "SELECT * from personal where idpersonal="+ idPersonal+";";
            //indicamos a mySQL el query que queremos ejecutar con la conexion previamente abierta
            MySqlCommand comando = new MySqlCommand(querysql, conexionSQL.conex);
            MySqlDataReader valoresTabla = comando.ExecuteReader();
            if (valoresTabla.Read())
            {
                if (valoresTabla["Tipo_empleado"].ToString() == rol)
                {
                    if (valoresTabla["contrasena"].ToString() == contrasena)
                    {
                        IniciarSesionApp(idPersonal, rol, valoresTabla["idSucursal"].ToString());
                    }
                    else
                    {
                        alerta.InnerText = "Por favor, verifica tus datos.";
                        alerta.Attributes.Add("class", "alert alert-danger");
                    }
                }
                else
                {
                    alerta.InnerText = "Por favor, verifica tus datos.";
                    alerta.Attributes.Add("class", "alert alert-danger");
                }
            }
            else
            {
                alerta.InnerText = "Por favor, verifica tus datos.";
                alerta.Attributes.Add("class", "alert alert-danger");
            }





        }
        public void IniciarSesionApp(string idPersonal, string rolPersonal, string sucursal)
        {
            Application["personalActivo"] = true;
            Application["nombreRolPersonalActivo"] = rolPersonal;
            Application["idPersonalActivo"] = idPersonal;
            Application["idSucursal"] = sucursal;
            RedirigirAWeb();
        }
        public void RedirigirAWeb()
        {
            if((bool)Application["personalActivo"] == false)
            {
                Response.Redirect("login.aspx");
            }
            if((string)Application["nombreRolPersonalActivo"] == "Administrador")
            {
                //redirigir a la pagina de administrador
                Response.Redirect("Pagina_Administrador.aspx");

            }
            else if((string)Application["nombreRolPersonalActivo"] == "Mesero")
            {
                //redirigir a la pagina de meseros
                Response.Redirect("Pagina_Meseros.aspx");
            }

        }
        public void CerrarSesion()
        {
            Application["personalActivo"] = false;
            Application["nombreRolPersonalActivo"] = "";
            Application["idPersonalActivo"] = 0;
            Response.Redirect("index.aspx");
        }
    }
    
}